package org.grakovne.onyxbookshelf.domain;

import java.util.Arrays;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Cover extends RealmObject {

    @PrimaryKey
    private String id;

    private byte[] cover;

    public Cover() {
    }

    public Cover(byte[] cover) {
        setCover(cover);
    }

    public String getId() {
        return id;
    }

    public void setId() {
        this.id = UUID.randomUUID().toString();
    }

    public byte[] getCover() {
        return cover;
    }

    public void setCover(byte[] cover) {
        this.cover = cover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cover cover1 = (Cover) o;

        return Arrays.equals(cover, cover1.cover);

    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(cover);
    }

    @Override
    public String toString() {
        return "Cover{" +
                "id=" + id +
                ", cover=" + Arrays.toString(cover) +
                '}';
    }
}
