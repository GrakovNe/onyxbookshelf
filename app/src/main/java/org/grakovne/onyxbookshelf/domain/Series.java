package org.grakovne.onyxbookshelf.domain;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Series extends RealmObject {

    @PrimaryKey
    private String id;

    @Index
    private String name;

    public Series() {
    }

    public Series(String name) {
        setName(name);
    }

    public String getId() {
        return id;
    }

    public void setId() {
        this.id = name.toLowerCase();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Series series = (Series) o;

        return name.equals(series.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Series{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
