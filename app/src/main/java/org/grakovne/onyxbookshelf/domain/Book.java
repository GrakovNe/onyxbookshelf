package org.grakovne.onyxbookshelf.domain;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Book extends RealmObject {

    @PrimaryKey
    private String id;

    @Index
    private String title;

    @Index
    private int year;

    private Author author;
    private Genre genre;
    private Series series;

    private Date fileChanged;

    @Index
    private Date fileAdded;

    @Index
    private String fileName;

    @Index
    private String filePath;

    private Cover cover;

    public Book() {
    }

    public Book(String fileName, String filePath) {
        setFileName(fileName);
        setFilePath(filePath);
    }

    public String getId() {
        return id;
    }

    public void setId() {
        this.id = (filePath + fileChanged).toLowerCase();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Date getFileChanged() {
        return fileChanged;
    }

    public void setFileChanged(Date fileChanged) {
        this.fileChanged = fileChanged;
    }

    public Date getFileAdded() {
        return fileAdded;
    }

    public void setFileAdded(Date fileAdded) {
        this.fileAdded = fileAdded;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (year != book.year) return false;
        if (title != null ? !title.equals(book.title) : book.title != null) return false;
        if (author != null ? !author.equals(book.author) : book.author != null) return false;
        if (genre != null ? !genre.equals(book.genre) : book.genre != null) return false;
        if (series != null ? !series.equals(book.series) : book.series != null) return false;
        if (fileChanged != null ? !fileChanged.equals(book.fileChanged) : book.fileChanged != null)
            return false;
        if (fileAdded != null ? !fileAdded.equals(book.fileAdded) : book.fileAdded != null)
            return false;
        if (!fileName.equals(book.fileName)) return false;
        return filePath.equals(book.filePath);

    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + year;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (series != null ? series.hashCode() : 0);
        result = 31 * result + (fileChanged != null ? fileChanged.hashCode() : 0);
        result = 31 * result + (fileAdded != null ? fileAdded.hashCode() : 0);
        result = 31 * result + fileName.hashCode();
        result = 31 * result + filePath.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", author=" + author +
                ", genre=" + genre +
                ", series=" + series +
                ", fileChanged=" + fileChanged +
                ", fileAdded=" + fileAdded +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
