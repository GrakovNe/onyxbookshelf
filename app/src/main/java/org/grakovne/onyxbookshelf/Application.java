package org.grakovne.onyxbookshelf;

import android.os.Environment;

import java.io.File;

public class Application extends android.app.Application {

    public final static File internalBookBath = Environment.getExternalStorageDirectory();
    public final static File externalBookPath = new File("/mnt/external_sd/");

}
