package org.grakovne.onyxbookshelf.db.sorting;

import org.grakovne.onyxbookshelf.db.services.GenreService;
import org.grakovne.onyxbookshelf.domain.Genre;

import io.realm.RealmResults;
import io.realm.Sort;

public class GenreSortingManager {
    private static GenreSortingManager instance;

    private GenreService genreService;
    private SortType sortType;

    private GenreSortingManager(GenreService genreService) {
        sortType = SortType.BY_NAME;
        this.genreService = genreService;
    }

    public static GenreSortingManager getManager(GenreService genreService) {
        if (null == instance) {
            instance = new GenreSortingManager(genreService);
        }

        return instance;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public RealmResults<Genre> sortGenreList(RealmResults<Genre> genres) {
        RealmResults<Genre> results = genres;

        switch (sortType) {
            case BY_TITLE:
                results = genreService.sortAuthorsByFirstName(genres, Sort.ASCENDING);
                break;
        }

        return results;
    }
}
