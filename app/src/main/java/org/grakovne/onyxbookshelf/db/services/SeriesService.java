package org.grakovne.onyxbookshelf.db.services;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.repositories.SeriesRepository;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.exceptions.entity.EntityNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.entity.IncorrectEntityException;

import io.realm.RealmResults;
import io.realm.Sort;

public class SeriesService extends BaseService {
    private final String EXCEPTION_MESSAGE = "series";

    private SeriesRepository seriesRepository;

    public SeriesService() {
        seriesRepository = new SeriesRepository();
    }

    public Series createSeries(Series series) {
        checkSeries(series);
        return seriesRepository.saveSeries(series);
    }

    public int getCount() {
        return seriesRepository.getAll().size();
    }

    public void deleteSeries(Series series) {
        checkSeries(series);
        checkSeriesFound(series);

        seriesRepository.removeSeries(series);
    }

    public Series updateSeries(Series series) {
        deleteSeries(series);
        return createSeries(series);
    }

    public RealmResults<Series> getAll() {
        return seriesRepository.getAll();
    }

    public Series getSeriesByID(String ID) {
        Series series = seriesRepository.getSeriesByID(ID);
        checkSeries(series);
        return series;
    }

    public RealmResults<Series> getSeresByName(String name) {
        return seriesRepository.getSeriesByName(name);
    }

    public RealmResults<Series> findSeries(String searchQuery) {
        return seriesRepository.findSeries(searchQuery);
    }

    public RealmResults<Series> sortSeriesByName(RealmResults<Series> series, Sort sort) {
        return seriesRepository.sortSeriesByName(series, sort);
    }

    private void checkSeriesFound(Series series) {
        if (null == seriesRepository.getSeriesByID(series.getId())) {
            throw new EntityNotFoundException(EXCEPTION_MESSAGE);
        }
    }

    private void checkSeries(Series series) {
        if (null == series || StringUtils.isEmpty(series.getName())) {
            throw new IncorrectEntityException(EXCEPTION_MESSAGE);
        }
    }
}
