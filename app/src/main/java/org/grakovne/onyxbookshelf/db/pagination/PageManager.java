package org.grakovne.onyxbookshelf.db.pagination;

import java.util.List;

import io.realm.RealmObject;

public class PageManager {
    private static Page page;

    public static void paginateList(List<? extends RealmObject> collection, PageType pageType) {
        switch (pageType) {
            case GRID_PAGE:
                page = new GridPage(collection);
                break;
            case LIST_PAGE:
                page = new ListPage(collection);
        }
    }

    public static Page getPage() {
        return page;
    }
}
