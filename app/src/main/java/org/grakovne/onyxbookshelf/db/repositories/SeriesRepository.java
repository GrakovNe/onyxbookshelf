package org.grakovne.onyxbookshelf.db.repositories;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.domain.Series;

import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;

public class SeriesRepository extends BaseRepository {

    public SeriesRepository() {
        super();
    }

    public RealmResults<Series> getAll() {
        return realm.where(Series.class).findAll();
    }


    public Series saveSeries(Series series) {
        realm.beginTransaction();
        Series persistSeries = realm.copyToRealm(series);
        realm.commitTransaction();

        return persistSeries;
    }

    public void removeSeries(Series series) {
        series.deleteFromRealm();
    }

    public Series getSeriesByID(String ID) {
        return realm.where(Series.class).equalTo("id", ID).findFirst();
    }

    public RealmResults<Series> findSeries(String searchQuery) {
        RealmResults<Series> results;

        String capitalizedSearchQuery = StringUtils.capitalize(searchQuery);

        if (StringUtils.isEmpty(searchQuery)) {
            results = getAll();
        } else {
            results = realm.where(Series.class)
                    .contains("name", searchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("name", capitalizedSearchQuery, Case.INSENSITIVE)
                    .findAllSorted("name");
        }

        return results;
    }

    public RealmResults<Series> getSeriesByName(String name) {
        return realm.where(Series.class).equalTo("name", name).findAll();
    }

    public RealmResults<Series> sortSeriesByName(RealmResults<Series> series, Sort sort) {
        return series.sort("name", sort);
    }
}
