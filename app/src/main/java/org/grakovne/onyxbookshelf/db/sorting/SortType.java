package org.grakovne.onyxbookshelf.db.sorting;

public enum SortType {
    BY_TITLE,
    BY_YEAR,
    BY_NOVELTY,
    BY_FIRST_NAME,
    BY_NAME
}
