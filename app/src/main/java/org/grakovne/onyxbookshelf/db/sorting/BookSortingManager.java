package org.grakovne.onyxbookshelf.db.sorting;

import org.grakovne.onyxbookshelf.db.services.BookService;
import org.grakovne.onyxbookshelf.domain.Book;

import io.realm.RealmResults;
import io.realm.Sort;

public class BookSortingManager {
    private static BookSortingManager instance;

    private BookService bookService;
    private SortType sortType;

    private BookSortingManager(BookService bookService) {
        sortType = SortType.BY_TITLE;
        this.bookService = bookService;
    }

    public static BookSortingManager getManager(BookService bookService) {
        if (null == instance) {
            instance = new BookSortingManager(bookService);
        }

        return instance;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public RealmResults<Book> sortBookList(RealmResults<Book> books) {
        RealmResults<Book> results = books;

        switch (sortType) {
            case BY_TITLE:
                results = bookService.sortBooksByTitle(books, Sort.ASCENDING);
                break;

            case BY_YEAR:
                results = bookService.sortBooksByYear(books, Sort.DESCENDING);
                break;

            case BY_NOVELTY:
                results = bookService.sortBooksByDateAdded(books, Sort.DESCENDING);
                break;
        }

        return results;
    }
}
