package org.grakovne.onyxbookshelf.db.repositories;

import io.realm.Realm;

public class BaseRepository {
    protected Realm realm;

    public BaseRepository() {
        realm = Realm.getDefaultInstance();
    }

    public void clearDataBase() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }
}
