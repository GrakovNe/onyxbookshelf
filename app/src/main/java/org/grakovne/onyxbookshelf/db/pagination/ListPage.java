package org.grakovne.onyxbookshelf.db.pagination;

import java.util.List;

import io.realm.RealmObject;

public class ListPage extends Page {
    private final static int LIST_PAGE_SIZE = 9;

    public ListPage(List<? extends RealmObject> bookList) {
        super(bookList, LIST_PAGE_SIZE);
    }

    public static int getListPageSize() {
        return LIST_PAGE_SIZE;
    }

}
