package org.grakovne.onyxbookshelf.db.services;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.repositories.BookRepository;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.exceptions.entity.EntityNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.entity.IncorrectEntityException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import io.realm.Sort;

public class BookService extends BaseService {
    private final String EXCEPTION_MESSAGE = "book";
    private BookRepository bookRepository;

    public BookService() {
        bookRepository = new BookRepository();
    }

    public void createBooks(Iterable<Book> books) {
        bookRepository.saveBooks(books);
    }

    public RealmResults<Book> getAll() {
        return bookRepository.getAll();
    }

    public int getCount() {
        return bookRepository.getAll().size();
    }

    public void deleteBook(Book book) {
        checkBook(book);
        checkBookFound(book);

        bookRepository.removeBook(book);
    }

    public void deleteBook(RealmResults<Book> book) {
        bookRepository.removeBook(book);
    }

    public List<File> getBookFiles() {
        List<Book> books = getAll();
        List<File> bookFiles = new ArrayList<>(books.size());

        for (Book book : books) {
            bookFiles.add(new File(book.getFilePath()));
        }

        return bookFiles;
    }

    public Book updateBook(Book book) {
        deleteBook(book);
        return createBook(book);
    }

    public RealmResults<Book> getBookByTitle(String title) {
        return bookRepository.getBookByTitle(title);
    }

    public RealmResults<Book> getBookByYear(int year) {
        return bookRepository.getBookByYear(year);
    }

    public RealmResults<Book> getBookByAuthor(Author author) {
        return bookRepository.getBookByAuthor(author);
    }

    public RealmResults<Book> getBookByGenre(Genre genre) {
        return bookRepository.getBookByGenre(genre);
    }

    public RealmResults<Book> getBookBySeries(Series series) {
        return bookRepository.getBookBySeries(series);
    }

    public RealmResults<Book> getBookByFilePath(String filePath) {
        return bookRepository.getBookByFilePath(filePath);
    }

    public RealmResults<Book> getBookByFileName(String fileName) {
        return bookRepository.getBookByFileName(fileName);
    }

    public RealmResults<Book> sortBooksByTitle(RealmResults<Book> books, Sort sort) {
        return bookRepository.sortBooksByTitle(books, sort);
    }

    public RealmResults<Book> sortBooksByYear(RealmResults<Book> books, Sort sort) {
        return bookRepository.sortBooksByYear(books, sort);
    }

    public RealmResults<Book> sortBooksByDateAdded(RealmResults<Book> books, Sort sort) {
        return bookRepository.sortBooksByDateAdded(books, sort);
    }

    public RealmResults<Book> findBooksByTitle(String title) {
        return bookRepository.findBooksByTitle(title);
    }

    private void checkBookFound(Book book) {
        if (null == bookRepository.getBookByID(book.getId())) {
            throw new EntityNotFoundException(EXCEPTION_MESSAGE);
        }
    }

    private void checkBook(Book book) {
        if (null == book || StringUtils.isEmpty(book.getFileName()) || StringUtils.isEmpty(book.getFilePath())) {
            throw new IncorrectEntityException(EXCEPTION_MESSAGE);
        }
    }

    private Book createBook(Book book) {
        return bookRepository.saveBook(book);
    }
}
