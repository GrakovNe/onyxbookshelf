package org.grakovne.onyxbookshelf.db.repositories;

import org.grakovne.onyxbookshelf.domain.Cover;

import io.realm.RealmResults;

public class CoverRepository extends BaseRepository {

    public CoverRepository() {
        super();
    }

    public RealmResults<Cover> getAll() {
        return realm.where(Cover.class).findAll();
    }

    public Cover saveCover(Cover cover) {
        realm.beginTransaction();
        Cover persistCover = realm.copyToRealm(cover);
        realm.commitTransaction();

        return persistCover;
    }

    public void removeCover(Cover cover) {
        realm.beginTransaction();
        cover.deleteFromRealm();
        realm.commitTransaction();
    }

    public Cover getCoverByID(String ID) {
        return realm.where(Cover.class).equalTo("id", ID).findFirst();
    }

}
