package org.grakovne.onyxbookshelf.db.pagination;

import java.util.List;

import io.realm.RealmObject;

public class GridPage extends Page {
    private final static int GRID_PAGE_SIZE = 12;

    public GridPage(List<? extends RealmObject> bookList) {
        super(bookList, GRID_PAGE_SIZE);
    }

    public static int getGridColumnsSize() {
        return GRID_PAGE_SIZE / 3;
    }

    public static int getGridRowsSize() {
        return GRID_PAGE_SIZE / 4;
    }

}
