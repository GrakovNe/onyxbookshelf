package org.grakovne.onyxbookshelf.db.services;

import org.grakovne.onyxbookshelf.db.repositories.CoverRepository;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.exceptions.entity.EntityNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.entity.IncorrectEntityException;

import io.realm.RealmResults;

public class CoverService extends BaseService {
    private final String EXCEPTION_MESSAGE = "cover";

    private CoverRepository coverRepository;

    public CoverService() {
        coverRepository = new CoverRepository();
    }

    public Cover createCover(Cover cover) {
        checkCover(cover);
        return coverRepository.saveCover(cover);
    }

    public Cover createCover(byte[] image) {
        Cover cover = new Cover(image);
        return createCover(cover);
    }

    public void deleteCover(Cover cover) {
        checkCover(cover);
        checkCoverFound(cover);
        coverRepository.removeCover(cover);
    }

    public Cover updateCover(Cover cover) {
        deleteCover(cover);
        return createCover(cover);
    }

    public RealmResults<Cover> getAll() {
        RealmResults<Cover> covers = coverRepository.getAll();
        return covers;
    }


    public Cover getCoverByID(String ID) {
        Cover cover = coverRepository.getCoverByID(ID);
        checkCover(cover);

        return cover;
    }

    private void checkCoverFound(Cover cover) {
        if (null == coverRepository.getCoverByID(cover.getId())) {
            throw new EntityNotFoundException(EXCEPTION_MESSAGE);
        }
    }

    private void checkCover(Cover cover) {
        if (null == cover || null == cover.getCover() || 0 == cover.getCover().length) {
            throw new IncorrectEntityException(EXCEPTION_MESSAGE);
        }
    }
}
