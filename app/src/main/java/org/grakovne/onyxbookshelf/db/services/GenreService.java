package org.grakovne.onyxbookshelf.db.services;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.repositories.GenreRepository;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.exceptions.entity.EntityNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.entity.IncorrectEntityException;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

public class GenreService extends BaseService {
    private final String EXCEPTION_MESSAGE = "genre";

    private GenreRepository genreRepository;

    public GenreService() {
        genreRepository = new GenreRepository();
    }

    public int getCount() {
        return genreRepository.getAll().size();
    }

    public Genre createGenre(Genre genre) {
        checkGenre(genre);
        return genreRepository.saveGenre(genre);
    }

    public Genre createGenre(String genreName) {
        Genre genre = new Genre(genreName);
        return createGenre(genre);
    }

    public void deleteGenre(Genre genre) {
        checkGenre(genre);
        checkGenreFound(genre);

        genreRepository.removeGenre(genre);
    }

    public Genre updateGenre(Genre genre) {
        deleteGenre(genre);
        return createGenre(genre);
    }

    public RealmResults<Genre> getAll() {
        return genreRepository.getAll();
    }

    public Genre getGenreByID(String ID) {
        Genre genre = genreRepository.getGenreByID(ID);
        checkGenre(genre);

        return genre;
    }

    public Genre getGenreByName(String name) {
        Genre genre = genreRepository.getGenreByName(name);
        checkGenre(genre);

        return genre;
    }

    public RealmResults<Genre> sortAuthorsByFirstName(RealmResults<Genre> genres, Sort sort) {
        return genreRepository.sortGenresByName(genres, sort);
    }


    private void checkGenre(Genre genre) {
        if (null == genre || StringUtils.isEmpty(genre.getName())) {
            throw new IncorrectEntityException(EXCEPTION_MESSAGE);
        }
    }

    private void checkGenreFound(Genre genre) {
        if (null == genreRepository.getGenreByID(genre.getId())) {
            throw new EntityNotFoundException(EXCEPTION_MESSAGE);
        }
    }

    public RealmResults<? extends RealmObject> findGenres(String searchRequest) {
        return genreRepository.findGenre(searchRequest);
    }
}
