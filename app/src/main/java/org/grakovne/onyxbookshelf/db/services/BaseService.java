package org.grakovne.onyxbookshelf.db.services;

import org.grakovne.onyxbookshelf.db.repositories.BaseRepository;

public class BaseService {
    public void clearDataBase() {
        new BaseRepository().clearDataBase();
    }
}
