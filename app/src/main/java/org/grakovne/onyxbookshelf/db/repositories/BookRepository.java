package org.grakovne.onyxbookshelf.db.repositories;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;

import java.util.Date;

import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;

public class BookRepository extends BaseRepository {

    public BookRepository() {
        super();
    }

    public RealmResults<Book> getAll() {
        return realm.where(Book.class).findAll();
    }

    public Book saveBook(Book book) {
        realm.beginTransaction();
        Book persistBook = realm.copyToRealm(book);
        realm.commitTransaction();

        return persistBook;
    }

    public Iterable<Book> saveBooks(Iterable<Book> books) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(books);

        realm.commitTransaction();
        return books;
    }


    public void removeBook(Book book) {
        realm.beginTransaction();
        book.deleteFromRealm();
        realm.commitTransaction();
    }

    public void removeBook(RealmResults<Book> book) {
        realm.beginTransaction();
        book.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public Book getBookByID(String ID) {
        return realm.where(Book.class).equalTo("id", ID).findFirst();
    }

    public RealmResults<Book> getBookByTitle(String title) {
        return realm.where(Book.class).equalTo("title", title).findAll();
    }

    public RealmResults<Book> getBookByYear(int year) {
        return realm.where(Book.class).equalTo("year", year).findAll();
    }

    public RealmResults<Book> getBookByFileName(String fileName) {
        return realm.where(Book.class).equalTo("fileName", fileName).findAll();
    }

    public RealmResults<Book> getBookByFilePath(String filePath) {
        return realm.where(Book.class).equalTo("filePath", filePath).findAll();
    }

    public RealmResults<Book> getBookByFileChanged(Date date) {
        return realm.where(Book.class).equalTo("fileChanged", date).findAll();
    }

    public RealmResults<Book> getBookByAuthor(Author author) {
        return realm.where(Book.class).equalTo("author.id", author.getId()).findAll();
    }

    public RealmResults<Book> getBookByGenre(Genre genre) {
        return realm.where(Book.class).equalTo("genre.id", genre.getId()).findAll();
    }

    public RealmResults<Book> getBookBySeries(Series series) {
        return realm.where(Book.class).equalTo("series.id", series.getId()).findAll();
    }

    public RealmResults<Book> getBookSortedByTitle(Sort sort) {
        return realm.where(Book.class).findAllSorted("title", sort);
    }

    public RealmResults<Book> getBookSortedByYear(Sort sort) {
        return realm.where(Book.class).findAllSorted("year", sort);
    }

    public RealmResults<Book> getBookSortedByAuthor(Sort sort) {
        return realm.where(Book.class).findAllSorted("author.id", sort);
    }

    public RealmResults<Book> getBookSortedByGenre(Sort sort) {
        return realm.where(Book.class).findAllSorted("genre.id", sort);
    }

    public RealmResults<Book> getBookSortedBySeries(Sort sort) {
        return realm.where(Book.class).findAllSorted("series.id", sort);
    }

    public RealmResults<Book> getBookSortedByFileName(Sort sort) {
        return realm.where(Book.class).findAllSorted("fileName", sort);
    }

    public RealmResults<Book> getBookSortedByFilePath(Sort sort) {
        return realm.where(Book.class).findAllSorted("filePath", sort);
    }

    public RealmResults<Book> getBookSortedByFileChanged(Sort sort) {
        return realm.where(Book.class).findAllSorted("fileChanged", sort);
    }

    public RealmResults<Book> sortBooksByTitle(RealmResults<Book> books, Sort sort) {
        return books.sort("title", sort);
    }

    public RealmResults<Book> sortBooksByYear(RealmResults<Book> books, Sort sort) {
        return books.sort("year", sort);
    }

    public RealmResults<Book> sortBooksByDateAdded(RealmResults<Book> books, Sort sort) {
        return books.sort("fileAdded", sort);
    }

    public RealmResults<Book> findBooksByTitle(String title) {
        RealmResults<Book> results;

        if (StringUtils.isEmpty(title)) {
            results = getAll();
        } else {
            results = realm.where(Book.class)
                    .contains("title", title, Case.INSENSITIVE)
                    .or()
                    .contains("title", StringUtils.capitalize(title))
                    .findAllSorted("title");
        }

        return results;
    }
}
