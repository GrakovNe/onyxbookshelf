package org.grakovne.onyxbookshelf.db.repositories;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.domain.Genre;

import io.realm.Case;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

public class GenreRepository extends BaseRepository {

    public GenreRepository() {
        super();
    }

    public RealmResults<Genre> getAll() {
        return realm.where(Genre.class).findAll();
    }

    public Genre saveGenre(Genre genre) {
        realm.beginTransaction();
        Genre persistGenre = realm.copyToRealm(genre);
        realm.commitTransaction();

        return persistGenre;
    }

    public void removeGenre(Genre genre) {
        realm.beginTransaction();
        genre.deleteFromRealm();
        realm.commitTransaction();
    }

    public Genre getGenreByID(String ID) {
        return realm.where(Genre.class).equalTo("id", ID).findFirst();
    }

    public Genre getGenreByName(String name) {
        return realm.where(Genre.class).equalTo("name", name).findFirst();
    }

    public RealmResults<Genre> sortGenresByName(RealmResults<Genre> genres, Sort sort) {
        return genres.sort("name", sort);
    }

    public RealmResults<? extends RealmObject> findGenre(String searchQuery) {
        RealmResults<Genre> results;

        String capitalizedSearchQuery = StringUtils.capitalize(searchQuery);

        if (StringUtils.isEmpty(searchQuery)) {
            results = getAll();
        } else {
            results = realm.where(Genre.class)
                    .contains("name", searchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("name", capitalizedSearchQuery, Case.INSENSITIVE)
                    .findAllSorted("name");
        }

        return results;
    }
}
