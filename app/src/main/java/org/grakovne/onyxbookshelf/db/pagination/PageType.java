package org.grakovne.onyxbookshelf.db.pagination;

public enum PageType {
    GRID_PAGE,
    LIST_PAGE
}
