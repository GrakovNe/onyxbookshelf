package org.grakovne.onyxbookshelf.db.sorting;

import org.grakovne.onyxbookshelf.db.services.AuthorService;
import org.grakovne.onyxbookshelf.domain.Author;

import io.realm.RealmResults;
import io.realm.Sort;

public class AuthorSortingManager {
    private static AuthorSortingManager instance;

    private AuthorService authorService;
    private SortType sortType;

    private AuthorSortingManager(AuthorService authorService) {
        sortType = SortType.BY_TITLE;
        this.authorService = authorService;
    }

    public static AuthorSortingManager getManager(AuthorService authorService) {
        if (null == instance) {
            instance = new AuthorSortingManager(authorService);
        }
        return instance;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public RealmResults<Author> sortAuthorList(RealmResults<Author> authors) {
        RealmResults<Author> results = authors;

        switch (sortType) {
            case BY_FIRST_NAME:
                results = authorService.sortAuthorsByFirstName(authors, Sort.ASCENDING);
                break;
        }

        return results;
    }
}
