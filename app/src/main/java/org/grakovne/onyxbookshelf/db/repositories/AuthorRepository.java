package org.grakovne.onyxbookshelf.db.repositories;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.domain.Author;

import io.realm.Case;
import io.realm.RealmResults;
import io.realm.Sort;

public class AuthorRepository extends BaseRepository {

    public AuthorRepository() {
        super();
    }

    public RealmResults<Author> getAll() {
        return realm.where(Author.class).findAll();
    }

    public Author saveAuthor(Author author) {
        realm.beginTransaction();
        Author persistAuthor = realm.copyToRealm(author);
        realm.commitTransaction();

        return persistAuthor;
    }

    public void removeAuthor(Author author) {
        realm.beginTransaction();
        author.deleteFromRealm();
        realm.commitTransaction();
    }

    public Author getAuthorByID(String ID) {
        return realm.where(Author.class).equalTo("id", ID).findFirst();
    }

    public RealmResults<Author> getAuthorByFirstName(String firstName) {
        return realm.where(Author.class).equalTo("firstName", firstName, Case.INSENSITIVE).findAllSorted("firstName");
    }

    public RealmResults<Author> getAuthorByMiddleName(String middleName) {
        return realm.where(Author.class).equalTo("middleName", middleName, Case.INSENSITIVE).findAllSorted("middleName");
    }

    public RealmResults<Author> getAuthorByLastName(String lastName) {
        return realm.where(Author.class).equalTo("lastName", lastName, Case.INSENSITIVE).findAllSorted("lastName");
    }

    public RealmResults<Author> findAuthors(String searchQuery) {
        RealmResults<Author> results;

        String capitalizedSearchQuery = StringUtils.capitalize(searchQuery);

        if (StringUtils.isEmpty(searchQuery)) {
            results = getAll();
        } else {
            results = realm.where(Author.class)
                    .contains("firstName", searchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("middleName", searchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("lastName", searchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("firstName", capitalizedSearchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("middleName", capitalizedSearchQuery, Case.INSENSITIVE)
                    .or()
                    .contains("lastName", capitalizedSearchQuery, Case.INSENSITIVE)

                    .findAllSorted("firstName");
        }

        return results;
    }

    public RealmResults<Author> sortAuthorsByFirstName(RealmResults<Author> authors, Sort sort) {
        return authors.sort("firstName", sort);
    }

    public RealmResults<Author> sortAuthorsByLastName(RealmResults<Author> authors, Sort sort) {
        return authors.sort("lastName", sort);
    }

}
