package org.grakovne.onyxbookshelf.db.sorting;

import org.grakovne.onyxbookshelf.db.services.SeriesService;
import org.grakovne.onyxbookshelf.domain.Series;

import io.realm.RealmResults;
import io.realm.Sort;

public class SeriesSortingManager {
    private static SeriesSortingManager instance;

    private SeriesService seriesService;
    private SortType sortType;

    private SeriesSortingManager(SeriesService seriesService) {
        sortType = SortType.BY_NAME;
        this.seriesService = seriesService;
    }

    public static SeriesSortingManager getManager(SeriesService seriesService) {
        if (null == instance) {
            instance = new SeriesSortingManager(seriesService);
        }

        return instance;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public RealmResults<Series> sortSeriesList(RealmResults<Series> series) {
        RealmResults<Series> results = series;

        switch (sortType) {
            case BY_TITLE:
                results = seriesService.sortSeriesByName(series, Sort.ASCENDING);
                break;
        }

        return results;
    }
}
