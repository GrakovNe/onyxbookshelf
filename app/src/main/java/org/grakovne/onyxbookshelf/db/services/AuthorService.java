package org.grakovne.onyxbookshelf.db.services;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.repositories.AuthorRepository;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.exceptions.entity.EntityNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.entity.IncorrectEntityException;

import io.realm.RealmResults;
import io.realm.Sort;

public class AuthorService extends BaseService {
    private final String EXCEPTION_MESSAGE = "author";
    private AuthorRepository authorRepository;

    public AuthorService() {
        authorRepository = new AuthorRepository();
    }

    public int getCount() {
        return authorRepository.getAll().size();
    }

    public Author createAuthor(Author author) {
        checkAuthor(author);
        return authorRepository.saveAuthor(author);
    }

    public void deleteAuthor(Author author) {
        checkAuthor(author);
        checkAuthorFound(author);
        authorRepository.removeAuthor(author);
    }

    public Author updateAuthor(Author author) {
        deleteAuthor(author);
        return createAuthor(author);
    }

    public RealmResults<Author> getAll() {
        return authorRepository.getAll();
    }

    public Author getAuthorByID(String ID) {
        Author author = authorRepository.getAuthorByID(ID);
        checkAuthor(author);

        return author;
    }

    public RealmResults<Author> getAuthorByFirstName(String firstName) {
        return authorRepository.getAuthorByFirstName(firstName);
    }


    public RealmResults<Author> getAuthorByMiddleName(String middleName) {
        return authorRepository.getAuthorByMiddleName(middleName);
    }


    public RealmResults<Author> getAuthorByLastName(String lastName) {
        return authorRepository.getAuthorByLastName(lastName);
    }

    public RealmResults<Author> sortAuthorsByFirstName(RealmResults<Author> authors, Sort sort) {
        return authorRepository.sortAuthorsByFirstName(authors, sort);
    }

    public RealmResults<Author> sortAuthorsByLastName(RealmResults<Author> authors, Sort sort) {
        return authorRepository.sortAuthorsByLastName(authors, sort);
    }

    public RealmResults<Author> findAuthors(String searchQuery) {
        return authorRepository.findAuthors(searchQuery);
    }

    private void checkAuthorFound(Author author) {
        if (null == authorRepository.getAuthorByID(author.getId())) {
            throw new EntityNotFoundException(EXCEPTION_MESSAGE);
        }
    }

    private void checkAuthor(Author author) {
        if (null == author || StringUtils.isEmpty(author.getFirstName()) || StringUtils.isEmpty(author.getLastName())) {
            throw new IncorrectEntityException(EXCEPTION_MESSAGE);
        }
    }
}
