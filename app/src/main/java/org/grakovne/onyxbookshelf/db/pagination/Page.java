package org.grakovne.onyxbookshelf.db.pagination;

import java.util.List;

import io.realm.RealmObject;

public abstract class Page {
    protected int pageNumber;
    protected List<? extends RealmObject> collection;
    private int PAGE_SIZE;

    protected Page(List<? extends RealmObject> collection, int pageSize) {
        this.collection = collection;
        this.pageNumber = 0;
        this.PAGE_SIZE = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public List<? extends RealmObject> getPage(int pageNumber) {
        List<? extends RealmObject> resultList;

        if (collection.size() >= PAGE_SIZE * (pageNumber + 1)) {
            resultList = collection.subList(PAGE_SIZE * pageNumber, PAGE_SIZE * (pageNumber + 1));
        } else {
            resultList = collection.subList(PAGE_SIZE * pageNumber, collection.size());
        }

        return resultList;
    }

    public List<? extends RealmObject> getPage() {
        return getPage(pageNumber);
    }

    public List<? extends RealmObject> getNextPage() {
        return getPage(++pageNumber);
    }

    public List<? extends RealmObject> getPreviousPage() {
        return getPage(--pageNumber);
    }

    public boolean hasNext() {
        return collection.size() > (pageNumber + 1) * PAGE_SIZE;
    }

    public boolean hasPrevious() {
        return pageNumber > 0;
    }

    public int getPageCount() {
        return (int) Math.ceil((float) collection.size() / (float) PAGE_SIZE);
    }
}
