package org.grakovne.onyxbookshelf.localization;

import android.content.Context;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.domain.Genre;

public class GenreLocalizer {
    private final String noGenreString;
    private BidiMap genreLocalizationMap;

    public GenreLocalizer(Context context) {

        noGenreString = context.getResources().getString(R.string.no_genre);

        genreLocalizationMap = new DualHashBidiMap();
        genreLocalizationMap.put("sf_history", context.getResources().getString(R.string.sf_history));
        genreLocalizationMap.put("sf_action", context.getResources().getString(R.string.sf_action));
        genreLocalizationMap.put("sf_epic", context.getResources().getString(R.string.sf_epic));
        genreLocalizationMap.put("sf_heroic", context.getResources().getString(R.string.sf_heroic));
        genreLocalizationMap.put("sf_detective", context.getResources().getString(R.string.sf_detective));
        genreLocalizationMap.put("sf_cyberpunk", context.getResources().getString(R.string.sf_cyberpunk));
        genreLocalizationMap.put("sf_space", context.getResources().getString(R.string.sf_space));
        genreLocalizationMap.put("sf_social", context.getResources().getString(R.string.sf_social));
        genreLocalizationMap.put("sf_horror", context.getResources().getString(R.string.sf_horror));
        genreLocalizationMap.put("sf_humor", context.getResources().getString(R.string.sf_humor));
        genreLocalizationMap.put("sf_mystic", context.getResources().getString(R.string.sf_mystic));
        genreLocalizationMap.put("sf_postapocalyptic", context.getResources().getString(R.string.sf_postapocalyptic));
        genreLocalizationMap.put("sf_fantasy", context.getResources().getString(R.string.sf_fantasy));
        genreLocalizationMap.put("sf", context.getResources().getString(R.string.sf));
        genreLocalizationMap.put("sf_city", context.getResources().getString(R.string.sf_city));
        genreLocalizationMap.put("city_fantasy", context.getResources().getString(R.string.city_fantasy));
        genreLocalizationMap.put("det_classic", context.getResources().getString(R.string.det_classic));
        genreLocalizationMap.put("det_police", context.getResources().getString(R.string.det_police));
        genreLocalizationMap.put("det_action", context.getResources().getString(R.string.det_action));
        genreLocalizationMap.put("det_irony", context.getResources().getString(R.string.det_irony));
        genreLocalizationMap.put("det_history", context.getResources().getString(R.string.det_history));
        genreLocalizationMap.put("det_espionage", context.getResources().getString(R.string.det_espionage));
        genreLocalizationMap.put("det_crime", context.getResources().getString(R.string.det_crime));
        genreLocalizationMap.put("det_political", context.getResources().getString(R.string.det_political));
        genreLocalizationMap.put("det_maniac", context.getResources().getString(R.string.det_maniac));
        genreLocalizationMap.put("det_hard", context.getResources().getString(R.string.det_hard));
        genreLocalizationMap.put("thriller", context.getResources().getString(R.string.thriller));
        genreLocalizationMap.put("detective", context.getResources().getString(R.string.detective));
        genreLocalizationMap.put("prose", context.getResources().getString(R.string.prose));
        genreLocalizationMap.put("prose_classic", context.getResources().getString(R.string.prose_classic));
        genreLocalizationMap.put("prose_history", context.getResources().getString(R.string.prose_history));
        genreLocalizationMap.put("prose_contemporary", context.getResources().getString(R.string.prose_contemporary));
        genreLocalizationMap.put("prose_counter", context.getResources().getString(R.string.prose_counter));
        genreLocalizationMap.put("prose_rus_classic", context.getResources().getString(R.string.prose_rus_classic));
        genreLocalizationMap.put("prose_su_classics", context.getResources().getString(R.string.prose_su_classics));
        genreLocalizationMap.put("love_contemporary", context.getResources().getString(R.string.love_contemporary));
        genreLocalizationMap.put("love_history", context.getResources().getString(R.string.love_history));
        genreLocalizationMap.put("love_detective", context.getResources().getString(R.string.love_detective));
        genreLocalizationMap.put("love_short", context.getResources().getString(R.string.love_short));
        genreLocalizationMap.put("love_erotica", context.getResources().getString(R.string.love_erotica));
        genreLocalizationMap.put("adv_western", context.getResources().getString(R.string.adv_western));
        genreLocalizationMap.put("adv_history", context.getResources().getString(R.string.adv_history));
        genreLocalizationMap.put("adv_indian", context.getResources().getString(R.string.adv_indian));
        genreLocalizationMap.put("adv_maritime", context.getResources().getString(R.string.adv_maritime));
        genreLocalizationMap.put("adv_geo", context.getResources().getString(R.string.adv_geo));
        genreLocalizationMap.put("adv_animal", context.getResources().getString(R.string.adv_animal));
        genreLocalizationMap.put("adventure", context.getResources().getString(R.string.adventure));
        genreLocalizationMap.put("child_tale", context.getResources().getString(R.string.child_tale));
        genreLocalizationMap.put("child_verse", context.getResources().getString(R.string.child_verse));
        genreLocalizationMap.put("child_prose", context.getResources().getString(R.string.child_prose));
        genreLocalizationMap.put("child_sf", context.getResources().getString(R.string.child_sf));
        genreLocalizationMap.put("child_det", context.getResources().getString(R.string.child_det));
        genreLocalizationMap.put("child_adv", context.getResources().getString(R.string.child_adv));
        genreLocalizationMap.put("child_education", context.getResources().getString(R.string.child_education));
        genreLocalizationMap.put("children", context.getResources().getString(R.string.children));
        genreLocalizationMap.put("poetry", context.getResources().getString(R.string.poetry));
        genreLocalizationMap.put("dramaturgy", context.getResources().getString(R.string.dramaturgy));
        genreLocalizationMap.put("antique_ant", context.getResources().getString(R.string.antique_ant));
        genreLocalizationMap.put("antique_european", context.getResources().getString(R.string.antique_european));
        genreLocalizationMap.put("antique_russian", context.getResources().getString(R.string.antique_russian));
        genreLocalizationMap.put("antique_east", context.getResources().getString(R.string.antique_east));
        genreLocalizationMap.put("antique_myths", context.getResources().getString(R.string.antique_myths));
        genreLocalizationMap.put("antique", context.getResources().getString(R.string.antique));
        genreLocalizationMap.put("sci_history", context.getResources().getString(R.string.sci_history));
        genreLocalizationMap.put("sci_psychology", context.getResources().getString(R.string.sci_psychology));
        genreLocalizationMap.put("sci_culture", context.getResources().getString(R.string.sci_culture));
        genreLocalizationMap.put("sci_religion", context.getResources().getString(R.string.sci_religion));
        genreLocalizationMap.put("sci_philosophy", context.getResources().getString(R.string.sci_philosophy));
        genreLocalizationMap.put("sci_politics", context.getResources().getString(R.string.sci_politics));
        genreLocalizationMap.put("sci_business", context.getResources().getString(R.string.sci_business));
        genreLocalizationMap.put("sci_juris", context.getResources().getString(R.string.sci_juris));
        genreLocalizationMap.put("sci_linguistic", context.getResources().getString(R.string.sci_linguistic));
        genreLocalizationMap.put("sci_medicine", context.getResources().getString(R.string.sci_medicine));
        genreLocalizationMap.put("sci_phys", context.getResources().getString(R.string.sci_phys));
        genreLocalizationMap.put("sci_math", context.getResources().getString(R.string.sci_math));
        genreLocalizationMap.put("sci_chem", context.getResources().getString(R.string.sci_chem));
        genreLocalizationMap.put("sci_biology", context.getResources().getString(R.string.sci_biology));
        genreLocalizationMap.put("sci_tech", context.getResources().getString(R.string.sci_tech));
        genreLocalizationMap.put("science", context.getResources().getString(R.string.science));
        genreLocalizationMap.put("comp_www", context.getResources().getString(R.string.comp_www));
        genreLocalizationMap.put("comp_programming", context.getResources().getString(R.string.comp_programming));
        genreLocalizationMap.put("comp_hard", context.getResources().getString(R.string.comp_hard));
        genreLocalizationMap.put("comp_soft", context.getResources().getString(R.string.comp_soft));
        genreLocalizationMap.put("comp_db", context.getResources().getString(R.string.comp_db));
        genreLocalizationMap.put("comp_osnet", context.getResources().getString(R.string.comp_osnet));
        genreLocalizationMap.put("computers", context.getResources().getString(R.string.computers));
        genreLocalizationMap.put("ref_encyc", context.getResources().getString(R.string.ref_encyc));
        genreLocalizationMap.put("ref_dict", context.getResources().getString(R.string.ref_dict));
        genreLocalizationMap.put("ref_ref", context.getResources().getString(R.string.ref_ref));
        genreLocalizationMap.put("ref_guide", context.getResources().getString(R.string.ref_guide));
        genreLocalizationMap.put("reference", context.getResources().getString(R.string.reference));
        genreLocalizationMap.put("nonf_biography", context.getResources().getString(R.string.nonf_biography));
        genreLocalizationMap.put("nonf_publicism", context.getResources().getString(R.string.nonf_publicism));
        genreLocalizationMap.put("nonf_criticism", context.getResources().getString(R.string.nonf_criticism));
        genreLocalizationMap.put("design", context.getResources().getString(R.string.design));
        genreLocalizationMap.put("nonfiction", context.getResources().getString(R.string.nonfiction));
        genreLocalizationMap.put("religion_rel", context.getResources().getString(R.string.religion_rel));
        genreLocalizationMap.put("religion_esoterics", context.getResources().getString(R.string.religion_esoterics));
        genreLocalizationMap.put("religion_self", context.getResources().getString(R.string.religion_self));
        genreLocalizationMap.put("religion", context.getResources().getString(R.string.religion));
        genreLocalizationMap.put("sci_religion", context.getResources().getString(R.string.sci_religion));
        genreLocalizationMap.put("humor_anecdote", context.getResources().getString(R.string.humor_anecdote));
        genreLocalizationMap.put("humor_prose", context.getResources().getString(R.string.humor_prose));
        genreLocalizationMap.put("humor_verse", context.getResources().getString(R.string.humor_verse));
        genreLocalizationMap.put("humor", context.getResources().getString(R.string.humor));
        genreLocalizationMap.put("home_cooking", context.getResources().getString(R.string.home_cooking));
        genreLocalizationMap.put("home_pets", context.getResources().getString(R.string.home_pets));
        genreLocalizationMap.put("home_crafts", context.getResources().getString(R.string.home_crafts));
        genreLocalizationMap.put("home_entertain", context.getResources().getString(R.string.home_entertain));
        genreLocalizationMap.put("home_health", context.getResources().getString(R.string.home_health));
        genreLocalizationMap.put("home_garden", context.getResources().getString(R.string.home_garden));
        genreLocalizationMap.put("home_diy", context.getResources().getString(R.string.home_diy));
        genreLocalizationMap.put("home_sport", context.getResources().getString(R.string.home_sport));
        genreLocalizationMap.put("home_sex", context.getResources().getString(R.string.home));
    }

    public String getLocalizedGenre(Genre genre) {
        String resultGenre = noGenreString;
        if (null != genre) {
            resultGenre = genre.getName();

            if (null != genreLocalizationMap.get(genre.getName())) {
                resultGenre = (String) genreLocalizationMap.get(genre.getName());
            }
        }
        return resultGenre;
    }

    public String getOriginalGenre(String string) {
        return (String) genreLocalizationMap.getKey(StringUtils.capitalize(string));
    }


}

