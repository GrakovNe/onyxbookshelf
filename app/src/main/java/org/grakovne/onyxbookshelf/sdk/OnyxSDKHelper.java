package org.grakovne.onyxbookshelf.sdk;

import android.content.Context;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import com.onyx.android.sdk.api.device.EpdDeviceManager;
import com.onyx.android.sdk.api.device.epd.EpdController;
import com.onyx.android.sdk.api.device.epd.UpdateMode;
import com.onyx.android.sdk.utils.DeviceUtils;

public class OnyxSDKHelper {

    public static void updateView(View view) {
        if (isOnyxDevice()) {
            EpdDeviceManager.applyGCUpdate(view);
        }
    }

    private static boolean isOnyxDevice() {
        return DeviceUtils.isRkDevice();
    }
}
