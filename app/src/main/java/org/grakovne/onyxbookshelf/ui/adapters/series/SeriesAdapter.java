package org.grakovne.onyxbookshelf.ui.adapters.series;

import android.content.Context;
import android.graphics.BitmapFactory;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.ui.adapters.ContentAdapter;

import java.util.List;

public abstract class SeriesAdapter extends ContentAdapter {
    protected List<Series> seriesList;

    public SeriesAdapter(Context context) {
        this.context = context;
        stubImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.series_image);
    }

    public void setSeriesList(List<Series> seriesList) {
        this.seriesList = seriesList;
    }

    @Override
    public Object getItem(int position) {
        return seriesList.get(position);
    }

    @Override
    public int getCount() {
        if (null != seriesList) {
            return seriesList.size();
        }

        return 0;
    }
}
