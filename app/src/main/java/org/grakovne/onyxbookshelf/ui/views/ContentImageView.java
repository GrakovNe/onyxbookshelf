package org.grakovne.onyxbookshelf.ui.views;

import android.content.Context;

public class ContentImageView extends android.support.v7.widget.AppCompatImageView {

    public ContentImageView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final double IMAGE_RATIO = 1.618;

        int height = MeasureSpec.getSize(heightMeasureSpec);
        int width = (int) (height / IMAGE_RATIO);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        );
    }
}
