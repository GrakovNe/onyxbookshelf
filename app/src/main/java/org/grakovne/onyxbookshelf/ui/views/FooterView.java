package org.grakovne.onyxbookshelf.ui.views;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.ui.listeners.SelectPageListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FooterView extends RelativeLayout {

    private final String selectPageTitleString;
    private final String selectPageOkButtonString;
    private final String selectPageCancelButtonString;
    @BindView(R.id.footer_prev_page_button)
    ImageButton footerPrevPageButton;
    @BindView(R.id.footer_page_counter_view)
    TextView footerPageCounterView;
    @BindView(R.id.footer_next_page_button)
    ImageButton footerNextPageButton;
    private AlertDialog pageSelectAlertDialog;

    public FooterView(Context context, AttributeSet attrs) {
        super(context, attrs);

        selectPageTitleString = getResources().getString(R.string.dialog_page_select_title);
        selectPageOkButtonString = getResources().getString(R.string.dialog_page_select_ok_button_text);
        selectPageCancelButtonString = getResources().getString(R.string.dialog_cancel_button_text);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.bind(this);
    }

    public void updatePageCounter(int currentPage, int totalPages) {
        final String PAGE_DELIMITER = " / ";

        if (totalPages > 0) {
            StringBuilder builder = new StringBuilder();
            builder
                    .append(currentPage + 1)
                    .append(PAGE_DELIMITER)
                    .append(totalPages);

            footerPageCounterView.setText(builder.toString());
            setVisibility(VISIBLE);
        } else {
            setVisibility(INVISIBLE);
        }
    }

    public TextView getFooterPageCounterView() {
        return footerPageCounterView;
    }

    public ImageButton getFooterPrevPageButton() {
        return footerPrevPageButton;
    }

    public ImageButton getFooterNextPageButton() {
        return footerNextPageButton;
    }

    public AlertDialog createSearchAlertDialog(final SelectPageListener selectPageListener) {

        if (null == pageSelectAlertDialog) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(selectPageTitleString);

            final EditText editText = new EditText(getContext());
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            builder.setView(editText);

            builder.setPositiveButton(selectPageOkButtonString, (dialog, which) -> {
                int newPageNumber = Integer.valueOf(editText.getText().toString()) - 1;
                selectPageListener.selectPage(newPageNumber);
            });

            builder.setNegativeButton(selectPageCancelButtonString, (dialog, which) -> dialog.cancel());

            pageSelectAlertDialog = builder.create();
            pageSelectAlertDialog.setCanceledOnTouchOutside(false);
        }

        return pageSelectAlertDialog;
    }
}
