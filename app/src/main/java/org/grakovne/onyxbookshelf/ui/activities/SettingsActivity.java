package org.grakovne.onyxbookshelf.ui.activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity {

    @BindView(R.id.settings_rescan_library_button)
    Button settingsRescanLibraryButton;
    @BindView(R.id.settings_total_books_text)
    TextView settingsTotalBooksText;
    @BindView(R.id.settings_total_authors_text)
    TextView settingsTotalAuthorsText;
    @BindView(R.id.settings_total_series_text)
    TextView settingsTotalSeriesText;
    @BindView(R.id.settings_total_genres_text)
    TextView settingsTotalGenresText;
    @BindView(R.id.settings_last_library_update_text)
    TextView settingsLastLibraryUpdateText;
    @BindView(R.id.settings_about_library_text)
    TextView settingsAboutLibraryText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        updateLibraryInfo();
        setListeners();
    }

    @Override
    public void onBackPressed() {
        restartApp();
    }

    private void setListeners() {
        settingsRescanLibraryButton.setOnClickListener(v -> completeLibraryScanningAction());
    }

    private void updateLibraryInfo() {
        settingsAboutLibraryText.setText(getString(R.string.setings_about_library_hint));
        settingsTotalBooksText.setText(String.format("%s %d", getString(R.string.settings_total_books_prefix), bookService.getCount()));
        settingsTotalAuthorsText.setText(String.format("%s %d", getString(R.string.settings_total_authors_prefix), authorService.getCount()));
        settingsTotalGenresText.setText(String.format("%s %d", getString(R.string.settings_total_genres_prefix), genreService.getCount()));
        settingsTotalSeriesText.setText(String.format("%s %d", getString(R.string.settings_total_series_prefix), seriesService.getCount()));
        settingsLastLibraryUpdateText.setText(String.format("%s %s", getString(R.string.settings_last_update_prefix), settingsManager.getLastRescanDate()));
    }
}
