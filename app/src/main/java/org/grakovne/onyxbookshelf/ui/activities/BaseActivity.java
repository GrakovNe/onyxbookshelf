package org.grakovne.onyxbookshelf.ui.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;

import org.apache.commons.collections.CollectionUtils;
import org.grakovne.onyxbookshelf.Application;
import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.db.services.AuthorService;
import org.grakovne.onyxbookshelf.db.services.BookService;
import org.grakovne.onyxbookshelf.db.services.GenreService;
import org.grakovne.onyxbookshelf.db.services.SeriesService;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.scanners.FileScanner;
import org.grakovne.onyxbookshelf.scanners.LibraryScanner;
import org.grakovne.onyxbookshelf.settings.SettingsManager;
import org.grakovne.onyxbookshelf.ui.dialogs.BookUpdatingDialog;
import org.grakovne.onyxbookshelf.ui.dialogs.listeners.ScanningDialogListener;

import java.io.File;
import java.util.Date;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

abstract class BaseActivity extends Activity {
    protected final static String BOOKS_CONTENT_TYPE = "text/";
    protected String TAG = "Onyx Bookshelf";

    protected BookService bookService;
    protected AuthorService authorService;
    protected SeriesService seriesService;
    protected GenreService genreService;

    protected SettingsManager settingsManager;

    private LibraryScanner libraryScanner;

    private Integer APPLICATION_INTENT_CODE = 6695669;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final String REALM_FILE_NAME = "OnyxBookShelfDataBase__v1.realm";

        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(REALM_FILE_NAME)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);

        bookService = new BookService();
        seriesService = new SeriesService();
        genreService = new GenreService();
        authorService = new AuthorService();

        settingsManager = SettingsManager.getManager(getApplication());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.global_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int selectedItemId = item.getItemId();

        switch (selectedItemId) {
            case R.id.main_menu_dash_item:
                restartApp();
                break;

            case R.id.main_menu_settings_item:
                switchActivity(SettingsActivity.class);
                break;
            case R.id.main_menu_about_item:
                switchActivity(AboutActivity.class);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Realm.getDefaultInstance().close();
        Realm.compactRealm(Realm.getDefaultInstance().getConfiguration());
    }

    protected void switchActivity(Class targetActivity) {
        Intent intent = new Intent(this, targetActivity);
        startActivity(intent);
    }

    protected boolean isLibraryIsEmpty() {
        return bookService.getCount() == 0;
    }

    protected AlertDialog getBookScanningDialog(String title, String message, final ScanningDialogListener event) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(R.string.reascan_library_query_yes_button, (dialogInterface, i) -> event.onPositiveButtonClick());
        builder.setNegativeButton(R.string.reascan_library_query_no_button, (dialogInterface, i) -> dialogInterface.cancel());

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    protected void updateLibrary() {
        Log.d(TAG, "Library updating...");

        if (isLibraryIsEmpty()) {
            getBookScanningDialog(getString(R.string.dash_initial_scan_title), getString(R.string.dash_iniital_scan_message),
                    this::completeLibraryScanningAction).show();
        } else {
            incrementalLibraryScanning();
        }

        settingsManager.setLastRescanDate(new Date());
    }

    protected void incrementalLibraryScanning() {
        FileScanner scanner = new FileScanner();
        List<File> filesInStorage = scanner.getBookFileRecursively(Application.internalBookBath);

        if (Application.externalBookPath.exists()) {
            filesInStorage.addAll(scanner.getBookFileRecursively(Application.externalBookPath));
        }

        List<File> filesInDataBase = bookService.getBookFiles();

        List<File> union = (List<File>) CollectionUtils.intersection(filesInDataBase, filesInStorage);

        filesInDataBase.removeAll(union);
        filesInStorage.removeAll(union);

        if (!filesInDataBase.isEmpty()) {
            for (File file : filesInDataBase) {
                RealmResults<Book> book = bookService.getBookByFilePath(file.getAbsolutePath());
                bookService.deleteBook(book);
            }
        }

        if (!filesInStorage.isEmpty()) {
            incrementalLibraryScanningAction(filesInStorage);
        }
    }

    protected void incrementalLibraryScanningAction(List<File> files) {
        getLibraryScanner().startBookFileListScan(files);
    }

    protected void completeLibraryScanningAction() {
        getLibraryScanner().startCompletelyRescan();
    }

    protected LibraryScanner getLibraryScanner() {
        if (null == libraryScanner) {
            final BookUpdatingDialog bookUpdatingDialog = new BookUpdatingDialog(this);
            final LibraryScanner scanner = new LibraryScanner();

            scanner.setScanningFinishedListener(() -> runOnUiThread(bookUpdatingDialog::hideDialog));

            scanner.setBookScannedListener(() -> runOnUiThread(() ->
                    bookUpdatingDialog.setBookCount(scanner.getBooksCount()))
            );

            scanner.setScanningStartedListener(bookUpdatingDialog::showDialog);
            libraryScanner = scanner;

        }
        return libraryScanner;
    }

    protected void restartApp() {
        Intent dashActivityIntent = new Intent(getApplicationContext(), DashActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                APPLICATION_INTENT_CODE,
                dashActivityIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) getApplication().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 10, pendingIntent);
        System.exit(0);
    }
}
