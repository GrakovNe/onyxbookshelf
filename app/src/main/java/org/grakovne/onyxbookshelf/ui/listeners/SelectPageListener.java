package org.grakovne.onyxbookshelf.ui.listeners;

public interface SelectPageListener {
    void selectPage(int pageNumber);
}
