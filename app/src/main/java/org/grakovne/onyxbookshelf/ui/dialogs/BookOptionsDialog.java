package org.grakovne.onyxbookshelf.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.widget.Toast;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.db.services.BookService;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.ui.dialogs.listeners.DeleteBookListener;

import java.io.File;

public class BookOptionsDialog extends AlertDialog {

    private final Book book;
    private final Context context;
    private final BookService bookService;
    private final DeleteBookListener listener;

    private AlertDialog currentInstance;

    private final String[] BOOK_OPTIONS = {
            getContext().getString(R.string.hide_book_option_item),
            getContext().getString(R.string.delete_book_option_item)
    };

    public BookOptionsDialog(Context context, Book book, DeleteBookListener listener) {
        super(context, false, null);
        this.book = book;
        this.context = context;
        this.listener = listener;

        bookService = new BookService();
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(null == book.getTitle() ? book.getFileName() : book.getTitle());

        builder.setItems(BOOK_OPTIONS, (dialogInterface, i) -> {

            String selectedOption = BOOK_OPTIONS[i];

            if (selectedOption.equals(getContext().getString(R.string.hide_book_option_item))) {
                hideBook();
            }

            if (selectedOption.equals(getContext().getString(R.string.delete_book_option_item))) {
                deleteBook();
            }
            hideDialog();
        });

        currentInstance = builder.create();
        currentInstance.setCanceledOnTouchOutside(false);
        currentInstance.show();
    }

    private void hideBook() {
        bookService.deleteBook(book);
        if (null != listener) {
            listener.onBookDeleted(book);
        }
    }

    private void deleteBook() {

        File bookFile = new File(book.getFilePath());
        if (bookFile.exists()) {
            boolean isDeleted = bookFile.delete();

            if (!isDeleted) {
                Toast.makeText(context, "Can't delete book file", Toast.LENGTH_LONG).show();
            } else {
                hideBook();
            }
        }
    }

    private void hideDialog() {
        if (null != currentInstance) {
            currentInstance.hide();
        }
    }
}
