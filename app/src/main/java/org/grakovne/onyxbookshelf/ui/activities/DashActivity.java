package org.grakovne.onyxbookshelf.ui.activities;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.db.pagination.PageManager;
import org.grakovne.onyxbookshelf.db.services.AuthorService;
import org.grakovne.onyxbookshelf.db.services.BookService;
import org.grakovne.onyxbookshelf.db.services.GenreService;
import org.grakovne.onyxbookshelf.db.services.SeriesService;
import org.grakovne.onyxbookshelf.db.sorting.AuthorSortingManager;
import org.grakovne.onyxbookshelf.db.sorting.BookSortingManager;
import org.grakovne.onyxbookshelf.db.sorting.GenreSortingManager;
import org.grakovne.onyxbookshelf.db.sorting.SeriesSortingManager;
import org.grakovne.onyxbookshelf.db.sorting.SortType;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.localization.GenreLocalizer;
import org.grakovne.onyxbookshelf.sdk.OnyxSDKHelper;
import org.grakovne.onyxbookshelf.ui.adapters.ContentType;
import org.grakovne.onyxbookshelf.ui.views.ContentView;
import org.grakovne.onyxbookshelf.ui.views.FooterView;
import org.grakovne.onyxbookshelf.ui.views.HeaderView;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import io.realm.RealmObject;
import io.realm.RealmResults;

@SuppressWarnings("unchecked")
public class DashActivity extends BaseActivity {

    @BindView(R.id.dash_content_container)
    ContentView dashContentContainer;

    @BindView(R.id.dash_header)
    HeaderView dashHeader;

    @BindView(R.id.dash_footer)
    FooterView dashFooter;

    private RealmResults<? extends RealmObject> dashContent;
    private ContentType contentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_dash);
        ButterKnife.bind(this);

        dashContentContainer.setDeleteBookListener(book ->
                dashContentContainer.showContent(PageManager.getPage().getPage(), contentType)
        );

        dashContentContainer.post(() -> {
            initContentList();
            initViews();
            setListeners();
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (settingsManager.getLibraryStatupUpdate()) {
            updateLibrary();
        }

        OnyxSDKHelper.updateView(dashContentContainer);
    }

    private void initContentList() {
        contentType = ContentType.BOOKS;

        bookService = new BookService();
        authorService = new AuthorService();
        seriesService = new SeriesService();
        genreService = new GenreService();

        switch (contentType) {
            case BOOKS:
                dashContent = BookSortingManager.getManager(bookService).sortBookList(bookService.getAll());
                break;

            case AUTHORS:
                dashContent = AuthorSortingManager.getManager(authorService).sortAuthorList(authorService.getAll());
                break;

            case SERIES:
                dashContent = SeriesSortingManager.getManager(seriesService).sortSeriesList(seriesService.getAll());
                break;

            case GENRES:
                dashContent = GenreSortingManager.getManager(genreService).sortGenreList(genreService.getAll());
                break;
        }
    }

    private void setListeners() {
        dashFooter.getFooterNextPageButton().setOnClickListener(v -> {
            if (PageManager.getPage() != null && PageManager.getPage().hasNext()) {
                dashContentContainer.showContent(PageManager.getPage().getNextPage(), contentType);
                dashFooter.updatePageCounter(PageManager.getPage().getPageNumber(), PageManager.getPage().getPageCount());
            }
        });

        dashFooter.getFooterPrevPageButton().setOnClickListener(v -> {
            if (PageManager.getPage() != null && PageManager.getPage().hasPrevious()) {
                dashContentContainer.showContent(PageManager.getPage().getPreviousPage(), contentType);
                dashFooter.updatePageCounter(PageManager.getPage().getPageNumber(), PageManager.getPage().getPageCount());
            }
        });

        dashFooter.getFooterPageCounterView().setOnClickListener(view -> {
            AlertDialog selectPageDialog = dashFooter.createSearchAlertDialog(pageNumber -> {
                if (pageNumber <= PageManager.getPage().getPageCount()) {
                    PageManager.getPage().setPageNumber(pageNumber);
                    dashContentContainer.showContent(PageManager.getPage().getPage(), contentType);
                    dashFooter.updatePageCounter(PageManager.getPage().getPageNumber(), PageManager.getPage().getPageCount());
                }
            });

            selectPageDialog.show();
        });

        dashHeader.getHeaderShowStyleButton().setOnClickListener(v -> {
            dashHeader.setIconInverted(dashContentContainer.getPageType());
            dashContentContainer.checkoutViewInverted(contentType);

            updateContent();
        });

        dashHeader.setSortContentListener(sortType -> {
            if (sortType.equals(SortType.BY_TITLE) || sortType.equals(SortType.BY_YEAR) || (sortType.equals(SortType.BY_NOVELTY))) {
                BookSortingManager.getManager(bookService).setSortType(sortType);
            }

            if (sortType.equals(SortType.BY_FIRST_NAME)) {
                AuthorSortingManager.getManager(authorService).setSortType(sortType);
            }

            if (sortType.equals(SortType.BY_NAME)) {
                SeriesSortingManager.getManager(seriesService).setSortType(sortType);
                GenreSortingManager.getManager(genreService).setSortType(sortType);
            }

            updateContent();
        });

        dashHeader.getSettingsButton().setOnClickListener(view ->
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class))
        );

        dashHeader.setShowContentListener(newContentType -> {
            switch (newContentType) {
                case BOOKS:
                    dashContent = bookService.getAll();
                    break;

                case AUTHORS:
                    dashContent = authorService.getAll();
                    break;

                case SERIES:
                    dashContent = seriesService.getAll();
                    break;

                case GENRES:
                    dashContent = genreService.getAll();
                    break;
            }

            contentType = newContentType;
            dashHeader.setSortingOption(contentType);

            dashContentContainer.checkoutView(contentType);

            updateContent();
        });

        dashHeader.getHeaderSearchButton().setOnClickListener(v -> {
            AlertDialog alertDialog = dashHeader.createSearchAlertDialog(searchRequest -> {

                switch (contentType) {
                    case BOOKS:
                        dashContent = bookService.findBooksByTitle(searchRequest);
                        break;
                    case AUTHORS:
                        dashContent = authorService.findAuthors(searchRequest);
                        break;
                    case SERIES:
                        dashContent = seriesService.findSeries(searchRequest);
                        break;
                    case GENRES:
                        String originalGenreName = new GenreLocalizer(getApplicationContext()).getOriginalGenre(searchRequest);
                        dashContent = genreService.findGenres(originalGenreName);
                        break;
                }

                updateContent();
            });
            alertDialog.show();
        });

        dashContentContainer.setAuthorSelectListener(author -> {
            contentType = ContentType.BOOKS;

            dashContent = bookService.getBookByAuthor(author);
            dashHeader.setSortingOption(contentType);

            dashContentContainer.checkoutView(contentType);

            updateContent();
        });

        dashContentContainer.setSeriesSelectListener(series -> {
            contentType = ContentType.BOOKS;

            dashContent = bookService.getBookBySeries(series);
            dashHeader.setSortingOption(contentType);

            dashContentContainer.checkoutView(contentType);

            updateContent();
        });

        dashContentContainer.setGenreSelectListener(genre -> {
            contentType = ContentType.BOOKS;

            dashContent = bookService.getBookByGenre(genre);
            dashHeader.setSortingOption(contentType);

            dashContentContainer.checkoutView(contentType);

            updateContent();
        });

        dashContentContainer.setBookSelectListener(book -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setDataAndType(Uri.fromFile(new File(book.getFilePath())), BOOKS_CONTENT_TYPE);

            try {
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Log.d(TAG, ex.getMessage());
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_opening_book_message), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initViews() {
        System.gc();
        dashHeader.setSortingOption(contentType);
        dashContentContainer.checkoutView(contentType);
        dashHeader.setIcon(dashContentContainer.getPageType());

        updateContent();
    }

    private void updateBookList(RealmResults<Book> bookList) {
        bookList = BookSortingManager.getManager(bookService).sortBookList(bookList);

        PageManager.paginateList(bookList, dashContentContainer.getPageType());
        dashContentContainer.showBooks((List<Book>) PageManager.getPage().getPage());
    }

    private void updateAuthorList(RealmResults<Author> authorList) {
        authorList = AuthorSortingManager.getManager(authorService).sortAuthorList(authorList);

        PageManager.paginateList(authorList, dashContentContainer.getPageType());
        dashContentContainer.showAuthors((List<Author>) PageManager.getPage().getPage());
    }

    private void updateSeriesList(RealmResults<Series> seriesList) {
        seriesList = SeriesSortingManager.getManager(seriesService).sortSeriesList(seriesList);

        PageManager.paginateList(seriesList, dashContentContainer.getPageType());
        dashContentContainer.showSeries((List<Series>) PageManager.getPage().getPage());
    }

    private void updateGenresList(RealmResults<Genre> genresList) {
        genresList = GenreSortingManager.getManager(genreService).sortGenreList(genresList);

        PageManager.paginateList(genresList, dashContentContainer.getPageType());
        dashContentContainer.showGenres((List<Genre>) PageManager.getPage().getPage());
    }

    private void updateContent() {
        switch (contentType) {
            case BOOKS:
                updateBookList((RealmResults<Book>) dashContent);
                break;

            case AUTHORS:
                updateAuthorList((RealmResults<Author>) dashContent);
                break;

            case SERIES:
                updateSeriesList((RealmResults<Series>) dashContent);
                break;

            case GENRES:
                updateGenresList((RealmResults<Genre>) dashContent);
                break;
        }

        dashFooter.updatePageCounter(PageManager.getPage().getPageNumber(), PageManager.getPage().getPageCount());

        OnyxSDKHelper.updateView(dashContentContainer);

    }

}
