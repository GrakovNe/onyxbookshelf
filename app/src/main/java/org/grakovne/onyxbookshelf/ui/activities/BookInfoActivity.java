package org.grakovne.onyxbookshelf.ui.activities;

import android.os.Bundle;
import android.app.Activity;

import org.grakovne.onyxbookshelf.R;

public class BookInfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_info);
    }

}
