package org.grakovne.onyxbookshelf.ui.adapters.genres;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.db.pagination.GridPage;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.ui.views.ContentImageView;

public class GenreGridAdapter extends GenreAdapter {

    public GenreGridAdapter(Context context) {
        super(context);
    }

    private RelativeLayout createLayout() {
        final int LAYOUT_PADDING = 10;
        final int GRID_COLUMN_NUMBER = GridPage.getGridColumnsSize();
        final int GRID_ROW_NUMBER = GridPage.getGridRowsSize();

        int layoutPaddingPx = (int) (LAYOUT_PADDING * context.getResources().getDisplayMetrics().density);

        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new GridView.LayoutParams(viewWidth / GRID_COLUMN_NUMBER - 1, viewHeight / GRID_ROW_NUMBER - 1));
        relativeLayout.setPadding(0, layoutPaddingPx, 0, layoutPaddingPx);

        return relativeLayout;
    }

    private RelativeLayout setCoverImage(RelativeLayout layout) {

        ContentImageView imageView = new ContentImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        imageView.setImageBitmap(stubImage);
        layout.addView(imageView);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        imageView.setLayoutParams(layoutParams);

        return layout;
    }

    private RelativeLayout setGenreName(RelativeLayout layout, Genre genre) {
        final int TITLE_MAX_LINES = 2;
        final int TITLE_PADDING = 20;

        int titlePaddingPx = (int) (TITLE_PADDING * context.getResources().getDisplayMetrics().density);

        TextView textView = new TextView(context);
        textView.setMaxLines(TITLE_MAX_LINES);
        textView.setEllipsize(TextUtils.TruncateAt.END);

        String localizedGenre = genreLocalizer.getLocalizedGenre(genre);
        textView.setText(localizedGenre);

        textView.setGravity(Gravity.CENTER);
        textView.setPadding(titlePaddingPx, 0, titlePaddingPx, titlePaddingPx);

        layout.addView(textView);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        textView.setLayoutParams(layoutParams);

        return layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Genre genre = ((Genre) getItem(position));

        RelativeLayout resultLayout = (RelativeLayout) convertView;

        if (null == resultLayout) {
            resultLayout = createLayout();
        }

        resultLayout.removeAllViews();
        resultLayout = setCoverImage(resultLayout);
        resultLayout = setGenreName(resultLayout, genre);

        return resultLayout;
    }
}
