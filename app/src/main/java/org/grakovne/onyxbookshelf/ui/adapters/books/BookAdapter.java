package org.grakovne.onyxbookshelf.ui.adapters.books;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.commons.io.FilenameUtils;
import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.ui.adapters.ContentAdapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BookAdapter extends ContentAdapter {
    protected List<Book> bookList;
    private Map<String, Bitmap> extensionStubMap;

    public BookAdapter(Context context) {
        this.context = context;
        stubImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.book_no_cover_image);

        extensionStubMap = new HashMap<>();
        extensionStubMap.put("prc", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_prc));
        extensionStubMap.put("mobi", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_mobi));
        extensionStubMap.put("rtf", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_rtf));
        extensionStubMap.put("djvu", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_djvu));
        extensionStubMap.put("doc", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_doc));
        extensionStubMap.put("epub", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_epub));
        extensionStubMap.put("fb2", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_fb2));
        extensionStubMap.put("txt", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_txt));
        extensionStubMap.put("pdf", BitmapFactory.decodeResource(context.getResources(), R.mipmap.book_no_cover_pdf));
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public Object getItem(int position) {
        return bookList.get(position);
    }

    @Override
    public int getCount() {
        if (null != bookList) {
            return bookList.size();
        }

        return 0;
    }

    protected Bitmap getCoverStubByBookFormat(Book book) {
        String fileExtension = FilenameUtils.getExtension(book.getFilePath());

        Bitmap result = stubImage;

        if (null != extensionStubMap.get(fileExtension)) {
            result = extensionStubMap.get(fileExtension);
        }

        return result;
    }
}
