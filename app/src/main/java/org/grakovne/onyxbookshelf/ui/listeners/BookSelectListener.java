package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.domain.Book;

public interface BookSelectListener {
    void confirmSelection(Book book);
}
