package org.grakovne.onyxbookshelf.ui.adapters;

public enum ContentType {
    BOOKS,
    AUTHORS,
    GENRES,
    SERIES
}
