package org.grakovne.onyxbookshelf.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.db.pagination.PageType;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.ui.adapters.ContentType;
import org.grakovne.onyxbookshelf.ui.adapters.authors.AuthorAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.authors.AuthorGridAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.authors.AuthorListAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.books.BookAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.books.GridBookAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.books.ListBookAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.genres.GenreAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.genres.GenreGridAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.genres.GenreListAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.series.SeriesAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.series.SeriesGridAdapter;
import org.grakovne.onyxbookshelf.ui.adapters.series.SeriesListAdapter;
import org.grakovne.onyxbookshelf.ui.dialogs.BookOptionsDialog;
import org.grakovne.onyxbookshelf.ui.dialogs.listeners.DeleteBookListener;
import org.grakovne.onyxbookshelf.ui.listeners.AuthorSelectListener;
import org.grakovne.onyxbookshelf.ui.listeners.BookSelectListener;
import org.grakovne.onyxbookshelf.ui.listeners.GenreSelectListener;
import org.grakovne.onyxbookshelf.ui.listeners.SeriesSelectListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmObject;

public class ContentView extends LinearLayout {
    private final Context context;

    @BindView(R.id.dash_content_container)
    ContentView dashContentContainer;

    private AbsListView currentContentView;

    private AuthorSelectListener authorSelectListener;
    private BookSelectListener bookSelectListener;
    private SeriesSelectListener seriesSelectListener;
    private GenreSelectListener genreSelectListener;
    private DeleteBookListener deleteBookListener;

    public ContentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void setDeleteBookListener(DeleteBookListener deleteBookListener) {
        this.deleteBookListener = deleteBookListener;
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.bind(this);
    }

    private void checkoutBookGridView() {
        System.gc();
        currentContentView = getGridView();

        addView(currentContentView);

        BookAdapter gridBookAdapter = new GridBookAdapter(context);

        gridBookAdapter.setViewSize(getWidth(), getHeight());

        currentContentView.setAdapter(gridBookAdapter);

        currentContentView.setOnItemClickListener((adapterView, view, i, l) -> {
            Book book = (Book) currentContentView.getAdapter().getItem(i);
            bookSelectListener.confirmSelection(book);
        });

        currentContentView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            Book book = (Book) currentContentView.getAdapter().getItem(i);
            return showBookOptionsDialog(book);
        });

    }

    private void checkoutAuthorGridView() {
        System.gc();
        currentContentView = getGridView();

        addView(currentContentView);

        AuthorAdapter gridBookAdapter = new AuthorGridAdapter(context);
        gridBookAdapter.setViewSize(getWidth(), getHeight());
        currentContentView.setAdapter(gridBookAdapter);

        currentContentView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Author author = (Author) currentContentView.getAdapter().getItem(i);

                if (null != authorSelectListener) {
                    authorSelectListener.confirmSelection(author);
                }
            }
        });
    }

    private void checkoutSeriesGridView() {
        System.gc();
        currentContentView = getGridView();

        addView(currentContentView);

        SeriesAdapter gridSeriesAdapter = new SeriesGridAdapter(context);
        gridSeriesAdapter.setViewSize(getWidth(), getHeight());
        currentContentView.setAdapter(gridSeriesAdapter);

        currentContentView.setOnItemClickListener((adapterView, view, i, l) -> {
            Series series = (Series) currentContentView.getAdapter().getItem(i);

            if (null != seriesSelectListener) {
                seriesSelectListener.confirmSelection(series);
            }
        });
    }

    private void checkoutGenreGridView() {
        System.gc();
        currentContentView = getGridView();

        addView(currentContentView);

        GenreAdapter gridGenreAdapter = new GenreGridAdapter(context);
        gridGenreAdapter.setViewSize(getWidth(), getHeight());
        currentContentView.setAdapter(gridGenreAdapter);

        currentContentView.setOnItemClickListener((adapterView, view, i, l) -> {
            Genre genre = (Genre) currentContentView.getAdapter().getItem(i);

            if (null != genreSelectListener) {
                genreSelectListener.confirmSelection(genre);
            }
        });
    }

    private void checkoutBookListView() {
        final ListView bookListView = getListView();

        if (null != currentContentView && null != currentContentView.getParent()) {
            removeView(currentContentView);
        }

        currentContentView = bookListView;

        addView(bookListView);

        BookAdapter listBookAdapter = new ListBookAdapter(context);
        listBookAdapter.setViewSize(getWidth(), getHeight());

        bookListView.setAdapter(listBookAdapter);

        bookListView.setOnItemClickListener((adapterView, view, i, l) -> {
            Book book = (Book) currentContentView.getAdapter().getItem(i);
            bookSelectListener.confirmSelection(book);
        });

        currentContentView.setOnItemLongClickListener((adapterView, view, i, l) -> {
            Book book = (Book) currentContentView.getAdapter().getItem(i);
            return showBookOptionsDialog(book);
        });
    }

    private void checkoutAuthorListView() {
        System.gc();
        final ListView authorListView = getListView();

        if (null != currentContentView && null != currentContentView.getParent()) {
            removeView(currentContentView);
        }

        currentContentView = authorListView;

        addView(authorListView);

        AuthorAdapter listAuthorAdapter = new AuthorListAdapter(context);
        listAuthorAdapter.setViewSize(getWidth(), getHeight());

        authorListView.setAdapter(listAuthorAdapter);

        authorListView.setOnItemClickListener((adapterView, view, i, l) -> {
            Author author = (Author) authorListView.getAdapter().getItem(i);

            if (null != authorSelectListener) {
                authorSelectListener.confirmSelection(author);
            }
        });
    }

    private void checkoutSeriesListView() {
        System.gc();
        final ListView seriesListView = getListView();

        if (null != currentContentView && null != currentContentView.getParent()) {
            removeView(currentContentView);
        }

        currentContentView = seriesListView;

        addView(seriesListView);

        SeriesAdapter listSeriesAdapter = new SeriesListAdapter(context);
        listSeriesAdapter.setViewSize(getWidth(), getHeight());

        seriesListView.setAdapter(listSeriesAdapter);

        currentContentView.setOnItemClickListener((adapterView, view, i, l) -> {
            Series series = (Series) currentContentView.getAdapter().getItem(i);

            if (null != seriesSelectListener) {
                seriesSelectListener.confirmSelection(series);
            }
        });
    }

    private void checkoutGenreListView() {
        System.gc();
        final ListView seriesListView = getListView();

        if (null != currentContentView && null != currentContentView.getParent()) {
            removeView(currentContentView);
        }

        currentContentView = seriesListView;

        addView(seriesListView);

        GenreAdapter listSeriesAdapter = new GenreListAdapter(context);
        listSeriesAdapter.setViewSize(getWidth(), getHeight());

        seriesListView.setAdapter(listSeriesAdapter);

        currentContentView.setOnItemClickListener((adapterView, view, i, l) -> {
            Genre genre = (Genre) currentContentView.getAdapter().getItem(i);

            if (null != seriesSelectListener) {
                genreSelectListener.confirmSelection(genre);
            }
        });
    }

    private void checkoutGridView(ContentType contentType) {
        switch (contentType) {
            case BOOKS:
                checkoutBookGridView();
                break;
            case AUTHORS:
                checkoutAuthorGridView();
                break;
            case SERIES:
                checkoutSeriesGridView();
                break;
            case GENRES:
                checkoutGenreGridView();
                break;
        }
    }

    private void checkoutListView(ContentType contentType) {
        switch (contentType) {
            case BOOKS:
                checkoutBookListView();
                break;
            case AUTHORS:
                checkoutAuthorListView();
                break;
            case SERIES:
                checkoutSeriesListView();
                break;
            case GENRES:
                checkoutGenreListView();
                break;
        }
    }

    public void checkoutView(ContentType contentType) {
        switch (getPageType()) {
            case GRID_PAGE:
                checkoutGridView(contentType);
                break;
            case LIST_PAGE:
                checkoutListView(contentType);
                break;
        }
    }

    public void checkoutViewInverted(ContentType contentType) {
        switch (getPageType()) {
            case LIST_PAGE:
                checkoutGridView(contentType);
                break;
            case GRID_PAGE:
                checkoutListView(contentType);
                break;
        }
    }

    public void showBooks(List<Book> bookList) {
        BookAdapter bookAdapter = (BookAdapter) currentContentView.getAdapter();
        bookAdapter.setBookList(bookList);
        bookAdapter.notifyDataSetChanged();
    }

    public void showAuthors(List<Author> authorList) {
        AuthorAdapter authorAdapter = (AuthorAdapter) currentContentView.getAdapter();
        authorAdapter.setAuthorList(authorList);
        authorAdapter.notifyDataSetChanged();
    }

    public void showSeries(List<Series> seriesList) {
        SeriesAdapter seriesAdapter = (SeriesAdapter) currentContentView.getAdapter();
        seriesAdapter.setSeriesList(seriesList);
        seriesAdapter.notifyDataSetChanged();
    }

    public void showGenres(List<Genre> genreList) {
        GenreAdapter genreAdapter = (GenreAdapter) currentContentView.getAdapter();
        genreAdapter.setSeriesList(genreList);
        genreAdapter.notifyDataSetChanged();
    }

    public void showContent(List<? extends RealmObject> contentList, ContentType contentType) {
        switch (contentType) {
            case BOOKS:
                showBooks((List<Book>) contentList);
                break;

            case AUTHORS:
                showAuthors((List<Author>) contentList);
                break;

            case SERIES:
                showSeries((List<Series>) contentList);
                break;

            case GENRES:
                showGenres((List<Genre>) contentList);
                break;
        }
    }

    public PageType getPageType() {
        if (currentContentView instanceof GridView) {
            return PageType.GRID_PAGE;
        }

        if (currentContentView instanceof ListView) {
            return PageType.LIST_PAGE;
        }

        return PageType.GRID_PAGE;
    }

    public void setAuthorSelectListener(AuthorSelectListener authorSelectListener) {
        this.authorSelectListener = authorSelectListener;
    }

    public void setBookSelectListener(BookSelectListener bookSelectListener) {
        this.bookSelectListener = bookSelectListener;
    }

    public void setSeriesSelectListener(SeriesSelectListener seriesSelectListener) {
        this.seriesSelectListener = seriesSelectListener;
    }

    public void setGenreSelectListener(GenreSelectListener genreSelectListener) {
        this.genreSelectListener = genreSelectListener;
    }

    private ListView getListView() {
        return new ListView(context);
    }

    private GridView getGridView() {
        final int COLUMN_NUMBER = 4;

        final GridView authorGridView = new GridView(context);

        if (null != currentContentView && null != currentContentView.getParent()) {
            removeView(currentContentView);
        }

        authorGridView.setNumColumns(COLUMN_NUMBER);
        authorGridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        authorGridView.setGravity(Gravity.CENTER);

        return authorGridView;
    }

    private boolean showBookOptionsDialog(Book book) {
        BookOptionsDialog bookOptionsDialog = new BookOptionsDialog(context, book, deleteBookListener);
        bookOptionsDialog.showDialog();

        return true;
    }
}
