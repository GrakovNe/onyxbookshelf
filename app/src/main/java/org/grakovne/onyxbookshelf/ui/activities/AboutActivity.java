package org.grakovne.onyxbookshelf.ui.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.sdk.OnyxSDKHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutActivity extends BaseActivity {

    @BindView(R.id.version_text_about)
    TextView versionTextAbout;
    @BindView(R.id.author_text_about)
    TextView authorTextAbout;
    @BindView(R.id.author_name_text_about)
    TextView authorNameTextAbout;
    @BindView(R.id.address_text_about)
    TextView addressTextAbout;
    @BindView(R.id.year_text_about)
    TextView yearTextAbout;
    @BindView(R.id.author_text)
    LinearLayout authorText;
    @BindView(R.id.author_logo)
    ImageView authorLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
    }

    @Override
    protected void onPostResume() {
        OnyxSDKHelper.updateView(authorLogo);
        super.onPostResume();
    }
}
