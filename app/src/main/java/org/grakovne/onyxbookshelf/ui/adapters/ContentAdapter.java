package org.grakovne.onyxbookshelf.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.BaseAdapter;

public abstract class ContentAdapter extends BaseAdapter {
    protected Context context;

    protected int viewWidth;
    protected int viewHeight;

    protected Bitmap stubImage;

    public void setViewSize(int width, int height) {
        this.viewWidth = width;
        this.viewHeight = height;
    }



    @Override
    public long getItemId(int position) {
        return position;
    }
}
