package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.domain.Series;

public interface SeriesSelectListener {
    void confirmSelection(Series series);
}
