package org.grakovne.onyxbookshelf.ui.dialogs.listeners;

public interface ScanningDialogListener {
    void onPositiveButtonClick();
}
