package org.grakovne.onyxbookshelf.ui.listeners;

public interface SearchListener {
    void confirmSearch(String searchRequest);
}
