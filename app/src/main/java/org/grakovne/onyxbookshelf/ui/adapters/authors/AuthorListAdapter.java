package org.grakovne.onyxbookshelf.ui.adapters.authors;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.pagination.ListPage;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.ui.views.ContentImageView;

public class AuthorListAdapter extends AuthorAdapter {
    public AuthorListAdapter(Context context) {
        super(context);
    }

    private String getAuthorData(Author author) {
        StringBuilder builder = new StringBuilder();

        if (StringUtils.isNotEmpty(author.getFirstName())) {
            builder
                    .append(author.getFirstName())
                    .append(" ");
        }

        if (StringUtils.isNotEmpty(author.getMiddleName())) {
            builder
                    .append(author.getMiddleName())
                    .append(" ");
        }

        if (StringUtils.isNotEmpty(author.getLastName())) {
            builder.append(author.getLastName());
        }

        return builder.toString();
    }

    private LinearLayout createLayout() {
        final int LIST_ROW_NUMBER = ListPage.getListPageSize();

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        layout.setLayoutParams(new ListView.LayoutParams(viewWidth, viewHeight / LIST_ROW_NUMBER - (int) context.getResources().getDisplayMetrics().density));

        return layout;
    }

    private LinearLayout setAuthorImage(LinearLayout layout) {
        final int IMAGE_PADDING = 3;
        final int pxPadding = (int) (IMAGE_PADDING * context.getResources().getDisplayMetrics().density);

        Log.d("Onyx", String.valueOf(context.getResources().getDisplayMetrics().density));

        ContentImageView imageView = new ContentImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        imageView.setImageBitmap(stubImage);

        layout.addView(imageView);

        layout.setPadding(0, pxPadding, 0, pxPadding);

        return layout;
    }

    private LinearLayout setAuthorName(LinearLayout layout, Author author) {
        final int LAYOUT_PADDING = 10;
        int pxPadding = (int) (LAYOUT_PADDING * context.getResources().getDisplayMetrics().density);

        TextView textView = new TextView(context);
        textView.setText(getAuthorData(author));
        textView.setPadding(pxPadding, 0, 0, 0);
        layout.addView(textView);

        layout.setGravity(Gravity.CENTER_VERTICAL);

        return layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Author author = ((Author) getItem(position));

        LinearLayout resultLayout = (LinearLayout) convertView;

        if (null == resultLayout) {
            resultLayout = createLayout();
        }

        resultLayout.removeAllViews();
        resultLayout = setAuthorImage(resultLayout);
        resultLayout = setAuthorName(resultLayout, author);

        return resultLayout;
    }
}
