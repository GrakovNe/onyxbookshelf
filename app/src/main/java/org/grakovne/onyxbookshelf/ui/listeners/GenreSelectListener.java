package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.domain.Genre;

public interface GenreSelectListener {
    void confirmSelection(Genre genre);
}
