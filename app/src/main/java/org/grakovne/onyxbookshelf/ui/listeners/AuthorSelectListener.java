package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.domain.Author;

public interface AuthorSelectListener {
    void confirmSelection(Author author);
}
