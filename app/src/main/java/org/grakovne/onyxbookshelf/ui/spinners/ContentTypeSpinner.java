package org.grakovne.onyxbookshelf.ui.spinners;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

@SuppressLint("AppCompatCustomView")
public class ContentTypeSpinner extends Spinner {
    public ContentTypeSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void
    setSelection(int position, boolean animate) {
        setSelection(position);
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected && getOnItemSelectedListener() != null) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }
}
