package org.grakovne.onyxbookshelf.ui.views;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.db.pagination.PageType;
import org.grakovne.onyxbookshelf.db.sorting.SortType;
import org.grakovne.onyxbookshelf.ui.adapters.ContentType;
import org.grakovne.onyxbookshelf.ui.listeners.SearchListener;
import org.grakovne.onyxbookshelf.ui.listeners.ShowContentListener;
import org.grakovne.onyxbookshelf.ui.listeners.SortContentListener;
import org.grakovne.onyxbookshelf.ui.spinners.ContentTypeSpinner;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HeaderView extends LinearLayout {
    private final String searchTitleString;
    private final String searchOkButtonString;
    private final String searchCancelButtonString;

    private final Bitmap gridIcon;
    private final Bitmap listIcon;
    private final Map<String, SortType> sortTypeMap;
    private final Map<String, ContentType> contentTypeMap;
    private final Map<ContentType, Integer> sortingOptionsMap;

    @BindView(R.id.header_show_by_spinner)
    ContentTypeSpinner headerShowBySpinner;
    @BindView(R.id.header_sort_by_spinner)
    Spinner headerSortBySpinner;
    @BindView(R.id.header_show_style_button)
    ImageButton headerShowStyleButton;
    @BindView(R.id.settings_button)
    ImageButton settingsButton;
    @BindView(R.id.header_search_button)
    ImageButton headerSearchButton;
    private AlertDialog searchAlertDialog;

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        searchTitleString = getResources().getString(R.string.dialog_search_title);
        searchOkButtonString = getResources().getString(R.string.dialog_search_ok_button_text);
        searchCancelButtonString = getResources().getString(R.string.dialog_cancel_button_text);

        sortTypeMap = new HashMap<>();
        sortTypeMap.put(getResources().getString(R.string.sort_book_by_title), SortType.BY_TITLE);
        sortTypeMap.put(getResources().getString(R.string.sort_book_by_date), SortType.BY_YEAR);
        sortTypeMap.put(getResources().getString(R.string.sort_book_by_novelty), SortType.BY_NOVELTY);
        sortTypeMap.put(getResources().getString(R.string.sort_authors_by_first_name), SortType.BY_FIRST_NAME);

        contentTypeMap = new HashMap<>();
        contentTypeMap.put(getResources().getString(R.string.show_all_books), ContentType.BOOKS);
        contentTypeMap.put(getResources().getString(R.string.show_authors), ContentType.AUTHORS);
        contentTypeMap.put(getResources().getString(R.string.show_series), ContentType.SERIES);
        contentTypeMap.put(getResources().getString(R.string.show_genres), ContentType.GENRES);

        sortingOptionsMap = new HashMap<>();
        sortingOptionsMap.put(ContentType.BOOKS, R.array.sort_books_by);
        sortingOptionsMap.put(ContentType.AUTHORS, R.array.sort_authors_by);
        sortingOptionsMap.put(ContentType.SERIES, R.array.sort_series_by);
        sortingOptionsMap.put(ContentType.GENRES, R.array.sort_genres_by);

        gridIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.show_in_grid);
        listIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.show_in_list);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        ButterKnife.bind(this);
    }

    @Override
    public void onViewAdded(View child) {
        super.onViewAdded(child);
    }

    public ImageButton getHeaderShowStyleButton() {
        return headerShowStyleButton;
    }

    public ImageButton getHeaderSearchButton() {
        return headerSearchButton;
    }

    public AlertDialog createSearchAlertDialog(final SearchListener searchListener) {

        if (null == searchAlertDialog) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(searchTitleString);

            View contentView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.search_dialog_layout, null);

            EditText editText = contentView.findViewById(R.id.search_dialog_search_input);
            ImageButton clearButton = contentView.findViewById(R.id.search_dialog_clear_button);
            clearButton.setOnClickListener(v -> editText.setText(""));
            builder.setView(contentView);

            builder.setPositiveButton(searchOkButtonString, (dialog, which) -> searchListener.confirmSearch(editText.getText().toString()));
            builder.setNegativeButton(searchCancelButtonString, (dialog, which) -> dialog.cancel());

            searchAlertDialog = builder.create();
            searchAlertDialog.setCanceledOnTouchOutside(false);
        }

        return searchAlertDialog;
    }

    public ImageButton getSettingsButton() {
        return settingsButton;
    }

    public void setSortingOption(ContentType contentType) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(sortingOptionsMap.get(contentType)));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        headerSortBySpinner.setAdapter(spinnerArrayAdapter);
    }

    public void setSortContentListener(final SortContentListener sortContentListener) {
        headerSortBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sortContentListener.sortContent(sortTypeMap.get(parent.getSelectedItem()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setShowContentListener(final ShowContentListener showContentListener) {
        headerShowBySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                showContentListener.showContent(contentTypeMap.get(parent.getSelectedItem()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setIcon(PageType pageType) {
        switch (pageType) {
            case GRID_PAGE:
                headerShowStyleButton.setImageBitmap(gridIcon);
                break;
            case LIST_PAGE:
                headerShowStyleButton.setImageBitmap(listIcon);
                break;
        }
    }

    public void setIconInverted(PageType pageType) {
        switch (pageType) {
            case LIST_PAGE:
                headerShowStyleButton.setImageBitmap(gridIcon);
                break;
            case GRID_PAGE:
                headerShowStyleButton.setImageBitmap(listIcon);
                break;
        }
    }
}
