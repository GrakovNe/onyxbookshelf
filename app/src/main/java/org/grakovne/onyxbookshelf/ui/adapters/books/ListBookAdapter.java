package org.grakovne.onyxbookshelf.ui.adapters.books;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.pagination.ListPage;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.ui.views.ContentImageView;

public class ListBookAdapter extends BookAdapter {
    public ListBookAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Book book = ((Book) getItem(position));

        LinearLayout resultLayout = (LinearLayout) convertView;

        if (null == resultLayout) {
            resultLayout = createLayout();
        }

        resultLayout.removeAllViews();
        resultLayout = setCoverImage(resultLayout, book);
        resultLayout = setBookInfo(resultLayout, book);

        return resultLayout;
    }

    private LinearLayout createLayout() {
        final int LIST_ROW_NUMBER = ListPage.getListPageSize();

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        layout.setLayoutParams(new ListView.LayoutParams(viewWidth, viewHeight / LIST_ROW_NUMBER - (int) context.getResources().getDisplayMetrics().density));

        return layout;
    }

    private LinearLayout setCoverImage(LinearLayout layout, Book book) {
        final int COVER_PADDING = 3;

        ContentImageView imageView = new ContentImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        Cover cover = book.getCover();

        if (null != cover) {
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(cover.getCover(), 0, cover.getCover().length));
        } else {
            imageView.setImageBitmap(getCoverStubByBookFormat(book));
        }

        layout.addView(imageView);

        final int pxPadding = (int) (COVER_PADDING * context.getResources().getDisplayMetrics().density);
        layout.setPadding(0, pxPadding, 0, pxPadding);

        return layout;
    }

    private LinearLayout setBookInfo(LinearLayout layout, Book book) {

        LinearLayout bookDataLayout = createBookDataLayout();

        if (null != book.getAuthor()) {
            TextView authorTextView = getAuthorTextView(book);
            bookDataLayout.addView(authorTextView);
        }

        TextView titleTextView = getBookTitle(book);
        bookDataLayout.addView(titleTextView);

        layout.setGravity(Gravity.CENTER_VERTICAL);
        layout.addView(bookDataLayout);

        return layout;
    }

    private LinearLayout createBookDataLayout() {
        final int LAYOUT_PADDING = 10;

        int pxPadding = (int) (LAYOUT_PADDING * context.getResources().getDisplayMetrics().density);

        LinearLayout bookDataLayout = new LinearLayout(context);
        bookDataLayout.setOrientation(LinearLayout.VERTICAL);
        bookDataLayout.setPadding(pxPadding, 0, 0, 0);

        return bookDataLayout;
    }

    public void setViewSize(int width, int height) {
        this.viewWidth = width;
        this.viewHeight = height;
    }

    private TextView getAuthorTextView(Book book) {
        TextView resultView = new TextView(context);

        StringBuilder authorData = new StringBuilder();

        if (!StringUtils.isEmpty(book.getAuthor().getFirstName())) {
            authorData.append(book.getAuthor().getFirstName());
            authorData.append(" ");
        }

        if (!StringUtils.isEmpty(book.getAuthor().getMiddleName())) {
            authorData.append(book.getAuthor().getMiddleName());
            authorData.append(" ");
        }

        if (!StringUtils.isEmpty(book.getAuthor().getLastName())) {
            authorData.append(book.getAuthor().getLastName());
        }

        resultView.setText(authorData.toString());
        return resultView;
    }

    private TextView getBookTitle(Book book) {
        final int TITLE_MAX_LINES = 1;

        String bookTitle;
        if (!StringUtils.isEmpty(book.getTitle())) {
            bookTitle = book.getTitle();
        } else {
            bookTitle = book.getFileName();
            bookTitle = FilenameUtils.removeExtension(bookTitle);
        }

        TextView textView = new TextView(context);
        textView.setMaxLines(TITLE_MAX_LINES);
        textView.setEllipsize(TextUtils.TruncateAt.END);

        textView.setText(bookTitle);

        return textView;
    }

}
