package org.grakovne.onyxbookshelf.ui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.R;

import static org.grakovne.onyxbookshelf.R.string.settings_found_books_prefix;

public class BookUpdatingDialog extends AlertDialog {
    private TextView bookCountText;
    private AlertDialog currentInstance;

    public BookUpdatingDialog(Context context) {
        super(context);
    }

    public void setBookCount(int bookCount) {
        if (null != bookCountText) {
            bookCountText.setText(getContext().getString(settings_found_books_prefix) + " " + bookCount);
        }
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.scanning_dialog_title);

        builder.setView(getDialogContent());
        currentInstance = builder.create();
        currentInstance.setCanceledOnTouchOutside(false);
        currentInstance.show();
    }

    public void hideDialog() {
        if (null != currentInstance) {
            currentInstance.hide();
        }
    }

    private LinearLayout getDialogContent() {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        bookCountText = new TextView(getContext());
        bookCountText.setPadding(0, convertDpToPx(24), 0, convertDpToPx(24));
        bookCountText.setTextSize(convertDpToPx(14));
        bookCountText.setGravity(Gravity.CENTER);
        linearLayout.addView(bookCountText);

        return linearLayout;
    }

    private int convertDpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
    }

    private boolean firstsOrEveryFifth(int integer) {
        return integer < 10 || integer % 5 == 0;
    }
}
