package org.grakovne.onyxbookshelf.ui.adapters.series;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.grakovne.onyxbookshelf.db.pagination.ListPage;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.ui.views.ContentImageView;

public class SeriesListAdapter extends SeriesAdapter {

    public SeriesListAdapter(Context context) {
        super(context);
    }

    private String getSeriesData(Series series) {
        return series.getName();
    }

    private LinearLayout createLayout() {
        final int LIST_ROW_NUMBER = ListPage.getListPageSize();

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        layout.setLayoutParams(new ListView.LayoutParams(viewWidth, viewHeight / LIST_ROW_NUMBER - (int) context.getResources().getDisplayMetrics().density));

        return layout;
    }

    private LinearLayout setSeriesImage(LinearLayout layout) {
        final int IMAGE_PADDING = 3;
        final int pxPadding = (int) (IMAGE_PADDING * context.getResources().getDisplayMetrics().density);

        ContentImageView imageView = new ContentImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        imageView.setImageBitmap(stubImage);

        layout.addView(imageView);

        layout.setPadding(0, pxPadding, 0, pxPadding);

        return layout;
    }

    private LinearLayout setSeriesName(LinearLayout layout, Series series) {
        final int LAYOUT_PADDING = 10;
        int pxPadding = (int) (LAYOUT_PADDING * context.getResources().getDisplayMetrics().density);

        TextView textView = new TextView(context);
        textView.setText(getSeriesData(series));
        textView.setPadding(pxPadding, 0, 0, 0);
        layout.addView(textView);

        layout.setGravity(Gravity.CENTER_VERTICAL);

        return layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Series series = ((Series) getItem(position));

        LinearLayout resultLayout = (LinearLayout) convertView;

        if (null == resultLayout) {
            resultLayout = createLayout();
        }

        resultLayout.removeAllViews();
        resultLayout = setSeriesImage(resultLayout);
        resultLayout = setSeriesName(resultLayout, series);

        return resultLayout;
    }
}
