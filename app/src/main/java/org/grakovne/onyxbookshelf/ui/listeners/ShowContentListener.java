package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.ui.adapters.ContentType;

public interface ShowContentListener {
    void showContent(ContentType contentType);
}
