package org.grakovne.onyxbookshelf.ui.adapters.authors;

import android.content.Context;
import android.graphics.BitmapFactory;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.ui.adapters.ContentAdapter;

import java.util.List;

public abstract class AuthorAdapter extends ContentAdapter {

    private List<Author> authorList;

    public AuthorAdapter(Context context) {
        this.context = context;
        stubImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.author_image);
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }



    @Override
    public Object getItem(int position) {
        return authorList.get(position);
    }

    @Override
    public int getCount() {
        if (null != authorList) {
            return authorList.size();
        }

        return 0;
    }
}
