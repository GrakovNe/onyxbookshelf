package org.grakovne.onyxbookshelf.ui.dialogs.listeners;

import org.grakovne.onyxbookshelf.domain.Book;

public interface DeleteBookListener {
    void onBookDeleted(Book book);
}
