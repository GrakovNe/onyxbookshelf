package org.grakovne.onyxbookshelf.ui.adapters.books;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.db.pagination.GridPage;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.ui.views.ContentImageView;

public class GridBookAdapter extends BookAdapter {
    public GridBookAdapter(Context context) {
        super(context);
    }

    private RelativeLayout createLayout() {
        final int LAYOUT_PADDING = 10;
        final int GRID_COLUMN_NUMBER = GridPage.getGridColumnsSize();
        final int GRID_ROW_NUMBER = GridPage.getGridRowsSize();

        int layoutPaddingPx = (int) (LAYOUT_PADDING * context.getResources().getDisplayMetrics().density);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new GridView.LayoutParams(viewWidth / GRID_COLUMN_NUMBER - 1, viewHeight / GRID_ROW_NUMBER - 1));
        relativeLayout.setPadding(0, layoutPaddingPx, 0, layoutPaddingPx);

        return relativeLayout;
    }

    private RelativeLayout setCoverImage(RelativeLayout layout, Book book) {

        ContentImageView imageView = new ContentImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        Cover cover = book.getCover();

        if (null != cover) {
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(cover.getCover(), 0, cover.getCover().length));
        } else {
            imageView.setImageBitmap(getCoverStubByBookFormat(book));
        }

        layout.addView(imageView);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        imageView.setLayoutParams(layoutParams);

        return layout;
    }

    private RelativeLayout setBookTitle(RelativeLayout layout, Book book) {
        final int TITLE_MAX_LINES = 2;
        final int TITLE_PADDING = 20;

        int titlePaddingPx = (int) (TITLE_PADDING * context.getResources().getDisplayMetrics().density);

        if (null != book.getCover()) {
            return layout;
        }

        String bookTitle;

        if (!StringUtils.isEmpty(book.getTitle())) {
            bookTitle = book.getTitle();
        } else {
            bookTitle = FilenameUtils.removeExtension(book.getFileName());
        }

        TextView textView = new TextView(context);
        textView.setMaxLines(TITLE_MAX_LINES);
        textView.setEllipsize(TextUtils.TruncateAt.END);

        textView.setText(bookTitle);

        textView.setGravity(Gravity.CENTER);
        textView.setPadding(titlePaddingPx, 0, titlePaddingPx, titlePaddingPx);

        layout.addView(textView);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) textView.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        textView.setLayoutParams(layoutParams);

        return layout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Book book = ((Book) getItem(position));

        RelativeLayout resultLayout = (RelativeLayout) convertView;

        if (null == resultLayout) {
            resultLayout = createLayout();
        }

        resultLayout.removeAllViews();
        resultLayout = setCoverImage(resultLayout, book);
        resultLayout = setBookTitle(resultLayout, book);

        return resultLayout;
    }

}
