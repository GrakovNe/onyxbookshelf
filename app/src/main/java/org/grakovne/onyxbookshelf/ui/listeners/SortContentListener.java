package org.grakovne.onyxbookshelf.ui.listeners;

import org.grakovne.onyxbookshelf.db.sorting.SortType;

public interface SortContentListener {
    void sortContent(SortType sortType);
}
