package org.grakovne.onyxbookshelf.ui.adapters.genres;

import android.content.Context;
import android.graphics.BitmapFactory;

import org.grakovne.onyxbookshelf.R;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.localization.GenreLocalizer;
import org.grakovne.onyxbookshelf.ui.adapters.ContentAdapter;

import java.util.List;

public abstract class GenreAdapter extends ContentAdapter {

    protected List<Genre> genreList;

    protected GenreLocalizer genreLocalizer;

    public GenreAdapter(Context context) {
        this.context = context;
        stubImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.genre_image);
        genreLocalizer = new GenreLocalizer(context);
    }

    public void setSeriesList(List<Genre> genreList) {
        this.genreList = genreList;
    }

    @Override
    public Object getItem(int position) {
        return genreList.get(position);
    }

    @Override
    public int getCount() {
        if (null != genreList) {
            return genreList.size();
        }

        return 0;
    }
}
