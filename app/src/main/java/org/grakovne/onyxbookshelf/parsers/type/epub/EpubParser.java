package org.grakovne.onyxbookshelf.parsers.type.epub;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.grakovne.onyxbookshelf.parsers.BookParser;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.type.BookDTO;
import org.grakovne.onyxbookshelf.parsers.type.encoding.XmlEncodingDetector;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.exceptions.files.BookFleNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.files.NonParsableBookFileException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class EpubParser extends BookParser {

    public EpubParser(File bookFile) {
        super(bookFile);
    }

    public EpubParser(File bookFile, BookParserConfiguration configuration) {
        super(bookFile, configuration);
    }

    @Override
    public Book getBook() {
        if (!bookFile.exists()) {
            throw new BookFleNotFoundException();
        }

        try {
            BookDTO bookDTO = parseRawMetaDta();
            return buildBook(bookDTO);
        } catch (Exception e) {
            throw new NonParsableBookFileException();
        }

    }

    @Override
    protected Author buildAuthor(BookDTO bookDTO) {
        if (StringUtils.isEmpty(bookDTO.getAuthorFirstName())) {
            return null;
        }

        Author author = new Author();

        if (!StringUtils.isEmpty(bookDTO.getAuthorFirstName())) {
            author.setFirstName(WordUtils.capitalize(bookDTO.getAuthorFirstName().trim().toLowerCase()));
        }

        author.setId();
        return author;
    }

    @Override
    protected Series buildSeries(BookDTO bookDTO) {
        if (!StringUtils.isEmpty(bookDTO.getSeries())) {
            Series series = new Series();
            series.setName(bookDTO.getSeries());
            series.setId();
            return series;
        }

        return null;
    }

    @Override
    protected String buildTitle(BookDTO bookDTO) {
        if (!StringUtils.isEmpty(bookDTO.getTitle())) {
            return WordUtils.capitalize(bookDTO.getTitle().trim());
        }

        return buildFileName(bookFile);
    }

    @Override
    protected Cover buildCover(BookDTO bookDTO) {
        if (StringUtils.isEmpty(bookDTO.getCoverFileName())) {
            return null;
        }

        InputStream contentStream;
        ZipFile zipFile;

        try {
            zipFile = new ZipFile(bookFile.getAbsoluteFile());
            contentStream = zipFile.getInputStream(zipFile.getEntry("OEBPS/images/" + bookDTO.getCoverFileName()));

            if (null == contentStream) {
                contentStream = zipFile.getInputStream(zipFile.getEntry("images/" + bookDTO.getCoverFileName()));
            }

            Cover cover = new Cover();
            cover.setCover(IOUtils.toByteArray(contentStream));
            cover.setId();
            return cover;

        } catch (Exception ex) {
            throw new NonParsableBookFileException();
        }
    }

    @Override
    protected Genre buildGenre(BookDTO bookDTO) {
        if (null != bookDTO.getGenre()) {
            String convertedGenre = bookDTO.getGenre().toLowerCase();
            Genre genre = new Genre();
            genre.setName(convertedGenre);
            genre.setId();
            return genre;
        }

        return null;
    }

    @Override
    protected int buildYear(BookDTO bookDTO) {
        if (null != bookDTO.getYear()) {
            String yearString = (bookDTO.getYear()).replaceAll("[^0-9]", "");

            if (yearString.length() > 4) {
                yearString = yearString.substring(0, 3);
            }

            try {
                return Integer.valueOf(yearString);
            } catch (NumberFormatException ex) {
                return 0;
            }
        }

        return 0;
    }

    @Override
    public BookDTO parseRawMetaDta() {
        InputStream contentStream;
        ZipFile zipFile;
        InputSource inputSource;

        try {
            zipFile = new ZipFile(bookFile.getAbsoluteFile());
            contentStream = zipFile.getInputStream(zipFile.getEntry("OEBPS/content.opf"));

            if (null == contentStream) {
                contentStream = zipFile.getInputStream(zipFile.getEntry("content.opf"));
            }

            String encoding = new XmlEncodingDetector().getEncoding(contentStream);
            InputStreamReader inputStreamReader = new InputStreamReader(contentStream, encoding);
            inputSource = new InputSource(inputStreamReader);

        } catch (IOException | NullPointerException ex) {
            throw new NonParsableBookFileException();
        }

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

        try {
            EpubParserHandler epubParserHandler = new EpubParserHandler(getConfiguration());
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(inputSource, epubParserHandler);

            return epubParserHandler.getBookDTO();

        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new NonParsableBookFileException();
        }
    }
}
