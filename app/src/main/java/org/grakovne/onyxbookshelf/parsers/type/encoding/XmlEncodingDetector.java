package org.grakovne.onyxbookshelf.parsers.type.encoding;

import org.grakovne.onyxbookshelf.exceptions.files.BookFleNotFoundException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class XmlEncodingDetector {

    public String getEncoding(File bookFile) {
        try {
            FileInputStream fileInputStream = new FileInputStream(bookFile);
            return getEncoding(fileInputStream);
        } catch (IOException e) {
            throw new BookFleNotFoundException();
        }
    }

    public String getEncoding(InputStream inputStream) {
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String xmlServiceLine = bufferedReader.readLine();
            xmlServiceLine = xmlServiceLine.substring(xmlServiceLine.indexOf("<?xml"), xmlServiceLine.indexOf("?>")).toLowerCase();

            String ENCODING_PREFIX = "encoding";
            xmlServiceLine = xmlServiceLine.substring(xmlServiceLine.indexOf(ENCODING_PREFIX) + ENCODING_PREFIX.length());
            xmlServiceLine = xmlServiceLine.replaceAll("[<\"?>=]", "");
            return xmlServiceLine;
        } catch (IOException e) {
            throw new BookFleNotFoundException();
        }
    }
}
