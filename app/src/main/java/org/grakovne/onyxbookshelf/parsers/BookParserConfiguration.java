package org.grakovne.onyxbookshelf.parsers;

public class BookParserConfiguration {
    private boolean isParseTitle = true;
    private boolean isParseAuthor = true;
    private boolean isParseYear = true;
    private boolean isParseSeries = true;
    private boolean isParseGenre = true;
    private boolean isParseCover = true;

    public boolean isParseTitle() {
        return isParseTitle;
    }

    public BookParserConfiguration setParseTitle(boolean parseTitle) {
        isParseTitle = parseTitle;
        return this;
    }

    public BookParserConfiguration getDefaultConfiguration() {
        return new BookParserConfiguration();
    }

    public boolean isParseAuthor() {
        return isParseAuthor;
    }

    public BookParserConfiguration setParseAuthor(boolean parseAuthor) {
        isParseAuthor = parseAuthor;
        return this;
    }

    public boolean isParseYear() {
        return isParseYear;
    }

    public BookParserConfiguration setParseYear(boolean parseYear) {
        isParseYear = parseYear;
        return this;
    }

    public boolean isParseSeries() {
        return isParseSeries;
    }

    public BookParserConfiguration setParseSeries(boolean parseSeries) {
        isParseSeries = parseSeries;
        return this;
    }

    public boolean isParseGenre() {
        return isParseGenre;
    }

    public BookParserConfiguration setParseGenre(boolean parseGenre) {
        isParseGenre = parseGenre;
        return this;
    }

    public boolean isParseCover() {
        return isParseCover;
    }

    public BookParserConfiguration setParseCover(boolean parseCover) {
        isParseCover = parseCover;
        return this;
    }
}
