package org.grakovne.onyxbookshelf.parsers.type;

import org.grakovne.onyxbookshelf.parsers.BookParser;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.exceptions.files.NonParsableBookFileException;

import java.io.File;

public class DefaultParser extends BookParser {

    public DefaultParser(File bookFile, BookParserConfiguration configuration) {
        super(bookFile, configuration);
    }

    public DefaultParser(File bookFile) {
        super(bookFile);
    }

    @Override
    public Book getBook() {
        try {
            return buildBook(null);
        } catch (Exception ex) {
            throw new NonParsableBookFileException();
        }
    }

    @Override
    protected Author buildAuthor(BookDTO bookDTO) {
        return null;
    }

    @Override
    protected Series buildSeries(BookDTO bookDTO) {
        return null;
    }

    @Override
    protected String buildTitle(BookDTO bookDTO) {
        return buildFileName(bookFile);
    }

    @Override
    protected Cover buildCover(BookDTO bookDTO) {
        return null;
    }

    @Override
    protected Genre buildGenre(BookDTO bookDTO) {
        return null;
    }

    @Override
    protected int buildYear(BookDTO bookDTO) {
        return 0;
    }

    @Override
    protected BookDTO parseRawMetaDta() {
        return null;
    }

    @Override
    protected Book buildBook(BookDTO bookDTO) {
        Book book = new Book();
        book.setFileChanged(buildFileChanged(bookFile));
        book.setFilePath(buildFilePath(bookFile));
        book.setFileName(buildFileName(bookFile));
        book.setId();
        return book;
    }
}
