package org.grakovne.onyxbookshelf.parsers.factories;

import org.grakovne.onyxbookshelf.parsers.BookParser;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.exceptions.files.NonParsableBookFileException;

import java.io.File;

public class BookFactory {

    private final ParserFactory parserFactory;

    public BookFactory() {
        parserFactory = new ParserFactory();
    }

    public Book getBook(File filePath, BookParserConfiguration configuration) {
        try {
            BookParser parser = parserFactory.getParser(filePath, configuration);
            return parser.getBook();
        } catch (NonParsableBookFileException ex) {
            return parserFactory.getDefaultParser(filePath).getBook();
        }
    }
}
