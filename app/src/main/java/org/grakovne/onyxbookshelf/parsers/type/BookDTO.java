package org.grakovne.onyxbookshelf.parsers.type;

public class BookDTO {
    private String title;
    private String year;
    private String authorFirstName;
    private String authorMiddleName;
    private String authorLastName;
    private String genre;
    private String series;
    private String cover;
    private String coverFileName;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorMiddleName() {
        return authorMiddleName;
    }

    public void setAuthorMiddleName(String authorMiddleName) {
        this.authorMiddleName = authorMiddleName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getCoverFileName() {
        return coverFileName;
    }

    public void setCoverFileName(String coverFileName) {
        this.coverFileName = coverFileName;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", authorFirstName='" + authorFirstName + '\'' +
                ", authorMiddleName='" + authorMiddleName + '\'' +
                ", authorLastName='" + authorLastName + '\'' +
                ", genre='" + genre + '\'' +
                ", series='" + series + '\'' +
                ", cover='" + cover + '\'' +
                ", coverFileName='" + coverFileName + '\'' +
                '}';
    }
}
