package org.grakovne.onyxbookshelf.parsers.type.epub;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.type.BookDTO;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class EpubParserHandler extends DefaultHandler {

    private boolean isAuthorSection;
    private boolean isYearSection;

    private BookDTO bookDTO = new BookDTO();

    private String thisElement;

    private BookParserConfiguration configuration;

    public EpubParserHandler(BookParserConfiguration configuration) {
        this.configuration = configuration;
    }

    public BookDTO getBookDTO() {
        return bookDTO;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        switch (qName) {
            case "dc:date":
                if (configuration.isParseYear() && attributes.getValue("opf:event").equalsIgnoreCase("original-publication")) {
                    isYearSection = true;
                }
                break;

            case "dc:creator":
                if (configuration.isParseAuthor() && attributes.getValue("opf:role").contains("aut")) {
                    isAuthorSection = true;
                }
                break;

            case "meta":
                if (configuration.isParseCover() && attributes.getValue("name").equalsIgnoreCase("cover")) {
                    bookDTO.setCoverFileName(attributes.getValue("content"));
                }

                if (configuration.isParseSeries() && attributes.getValue("name").equalsIgnoreCase("calibre:series")) {
                    bookDTO.setSeries(attributes.getValue("content"));
                }

                break;

        }

        thisElement = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "dc:creator":
                isAuthorSection = false;
                break;

            case "dc:date":
                isYearSection = false;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        switch (thisElement) {
            case "dc:title":
                if (configuration.isParseTitle()) {
                    String newTitle = new String(ch, start, length).trim();
                    if (!StringUtils.isEmpty(newTitle)) {
                        bookDTO.setTitle(new String(ch, start, length));
                    }
                }
                break;

            case "dc:creator":
                if (configuration.isParseAuthor()) {
                    String newAuthor = new String(ch, start, length).trim();
                    if (!StringUtils.isEmpty(newAuthor)) {
                        bookDTO.setAuthorFirstName(newAuthor);
                    }
                }
                break;

            case "dc:subject":
                if (configuration.isParseGenre()) {
                    String newSubject = new String(ch, start, length).trim();
                    if (!StringUtils.isEmpty(newSubject)) {
                        bookDTO.setGenre(newSubject);
                    }
                }
                break;

            case "dc:date":
                if (configuration.isParseYear()) {
                    String newYear = new String(ch, start, length).trim();
                    if (!StringUtils.isEmpty(newYear)) {
                        bookDTO.setYear(newYear);
                    }
                }
                break;
        }
    }
}
