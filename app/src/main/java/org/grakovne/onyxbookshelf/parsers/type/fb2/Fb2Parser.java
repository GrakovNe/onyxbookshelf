package org.grakovne.onyxbookshelf.parsers.type.fb2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.grakovne.onyxbookshelf.parsers.BookParser;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.type.BookDTO;
import org.grakovne.onyxbookshelf.parsers.type.encoding.XmlEncodingDetector;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;
import org.grakovne.onyxbookshelf.exceptions.files.BookFleNotFoundException;
import org.grakovne.onyxbookshelf.exceptions.files.NonParsableBookFileException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

public class Fb2Parser extends BookParser {

    public Fb2Parser(File bookFile, BookParserConfiguration configuration) {
        super(bookFile, configuration);
    }

    @Override
    public Book getBook() {
        if (!bookFile.exists()) {
            throw new BookFleNotFoundException();
        }

        try {
            BookDTO bookDTO = parseRawMetaDta();
            return buildBook(bookDTO);
        } catch (Exception e) {
            throw new NonParsableBookFileException();
        }
    }

    @Override
    protected Author buildAuthor(BookDTO bookDTO) {
        if (StringUtils.isEmpty(bookDTO.getAuthorFirstName()) && StringUtils.isEmpty(bookDTO.getAuthorMiddleName()) && StringUtils.isEmpty(bookDTO.getAuthorLastName())) {
            return null;
        }

        Author author = new Author();

        if (!StringUtils.isEmpty(bookDTO.getAuthorFirstName())) {
            author.setFirstName(WordUtils.capitalize(bookDTO.getAuthorFirstName().trim().toLowerCase()));
        }

        if (!StringUtils.isEmpty(bookDTO.getAuthorLastName())) {
            author.setLastName(WordUtils.capitalize(bookDTO.getAuthorLastName().trim().toLowerCase()));
        }

        if (!StringUtils.isEmpty(bookDTO.getAuthorMiddleName())) {
            author.setMiddleName(WordUtils.capitalize(bookDTO.getAuthorMiddleName().trim().toLowerCase()));
        }

        author.setId();
        return author;
    }

    @Override
    protected Series buildSeries(BookDTO bookDTO) {
        if (!StringUtils.isEmpty(bookDTO.getSeries())) {
            Series series = new Series();
            series.setName(bookDTO.getSeries());
            series.setId();
            return series;
        }

        return null;
    }

    @Override
    protected String buildTitle(BookDTO bookDTO) {
        if (!StringUtils.isEmpty(bookDTO.getTitle())) {
            return WordUtils.capitalize(bookDTO.getTitle().trim());
        }
        return buildFileName(bookFile);
    }

    @Override
    protected Cover buildCover(BookDTO bookDTO) {
        if (StringUtils.isEmpty(bookDTO.getCover())) {
            return null;
        }

        try {
            System.gc();
            String encodedCover = resizeBase64Image(bookDTO.getCover());
            byte[] image = Base64.decode(encodedCover, Base64.NO_WRAP);
            Cover cover = new Cover();
            cover.setCover(image);
            cover.setId();
            return cover;
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }

    public String resizeBase64Image(String base64image) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inSampleSize = 2;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);

        image = Bitmap.createScaledBitmap(image, 400, 470, false);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 50, baos);

        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    @Override
    protected Genre buildGenre(BookDTO bookDTO) {
        if (null != bookDTO.getGenre()) {
            String convertedGenre = bookDTO.getGenre().toLowerCase();
            Genre genre = new Genre();
            genre.setName(convertedGenre);
            genre.setId();
            return genre;
        }

        return null;
    }

    @Override
    protected int buildYear(BookDTO bookDTO) {
        if (null != bookDTO.getYear()) {
            String yearString = (bookDTO.getYear()).replaceAll("[^0-9]", "");

            if (yearString.length() > 4) {
                yearString = yearString.substring(0, 3);
            }

            try {
                return Integer.valueOf(yearString);
            } catch (NumberFormatException ex) {
                return 0;
            }
        }

        return 0;
    }

    @Override
    protected BookDTO parseRawMetaDta() {
        Fb2ParserHandler fb2ParserHandler = new Fb2ParserHandler(getConfiguration());

        try {
            FileInputStream fileInputStream = new FileInputStream(bookFile);
            String encoding = new XmlEncodingDetector().getEncoding(bookFile);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, encoding);
            InputSource inputSource = new InputSource(inputStreamReader);

            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.newSAXParser().parse(inputSource, fb2ParserHandler);

            return fb2ParserHandler.getBookDTO();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new NonParsableBookFileException();
        }
    }
}
