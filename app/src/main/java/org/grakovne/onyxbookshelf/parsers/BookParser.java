package org.grakovne.onyxbookshelf.parsers;

import org.grakovne.onyxbookshelf.parsers.type.BookDTO;
import org.grakovne.onyxbookshelf.domain.Author;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.domain.Cover;
import org.grakovne.onyxbookshelf.domain.Genre;
import org.grakovne.onyxbookshelf.domain.Series;

import java.io.File;
import java.util.Date;

public abstract class BookParser {
    protected File bookFile;

    private BookParserConfiguration configuration;

    public BookParser(File bookFile, BookParserConfiguration configuration) {
        this.bookFile = bookFile;
        this.configuration = configuration;
    }

    public BookParser(File bookFile) {
        this(bookFile, new BookParserConfiguration().getDefaultConfiguration());
    }

    abstract public Book getBook();

    public BookParserConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(BookParserConfiguration configuration) {
        this.configuration = configuration;
    }

    protected String buildFilePath(File bookFile) {
        return bookFile.getPath();
    }

    protected String buildFileName(File bookFile) {
        return bookFile.getName();
    }

    protected Date buildFileChanged(File bookFile) {
        return new Date(bookFile.lastModified());
    }

    protected abstract Author buildAuthor(BookDTO bookDTO);

    protected abstract Series buildSeries(BookDTO bookDTO);

    protected abstract String buildTitle(BookDTO bookDTO);

    protected abstract Cover buildCover(BookDTO bookDTO);

    protected abstract Genre buildGenre(BookDTO bookDTO);

    protected abstract int buildYear(BookDTO bookDTO);

    protected abstract BookDTO parseRawMetaDta();

    protected Book buildBook(BookDTO bookDTO) {
        Book book = new Book();

        book.setTitle(buildTitle(bookDTO));

        book.setAuthor(buildAuthor(bookDTO));

        book.setYear(buildYear(bookDTO));

        book.setSeries(buildSeries(bookDTO));

        book.setGenre(buildGenre(bookDTO));

        book.setCover(buildCover(bookDTO));

        book.setFileChanged(buildFileChanged(bookFile));
        book.setFilePath(buildFilePath(bookFile));
        book.setFileName(buildFileName(bookFile));

        book.setFileAdded(new Date());

        book.setId();
        return book;
    }
}
