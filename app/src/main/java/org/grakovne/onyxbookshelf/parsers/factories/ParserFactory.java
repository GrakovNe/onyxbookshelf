package org.grakovne.onyxbookshelf.parsers.factories;

import org.apache.commons.io.FilenameUtils;
import org.grakovne.onyxbookshelf.parsers.BookParser;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.type.DefaultParser;
import org.grakovne.onyxbookshelf.parsers.type.epub.EpubParser;
import org.grakovne.onyxbookshelf.parsers.type.fb2.Fb2Parser;

import java.io.File;

public class ParserFactory {
    public BookParser getParser(File file, BookParserConfiguration configuration) {
        String fileExtension = FilenameUtils.getExtension(file.getAbsolutePath()).toLowerCase();
        switch (fileExtension) {
            case "fb2":
                return new Fb2Parser(file, configuration);
            case "epub":
                return new EpubParser(file, configuration);
            default:
                return new DefaultParser(file, configuration);
        }
    }

    public BookParser getDefaultParser(File file) {
        return new DefaultParser(file, new BookParserConfiguration().getDefaultConfiguration());
    }
}
