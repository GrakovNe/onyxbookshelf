package org.grakovne.onyxbookshelf.parsers.type.fb2;

import org.apache.commons.lang3.StringUtils;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.type.BookDTO;
import org.grakovne.onyxbookshelf.exceptions.files.NonParsableBookFileException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Fb2ParserHandler extends DefaultHandler {
    private BookDTO bookDTO;

    private String thisElement;

    private boolean isAuthorSection;
    private boolean isTitleInfoSection;
    private boolean isPublishInfoSection;
    private boolean isCoverSection;
    private boolean isCoverImageTag;

    private String coverImage;
    private StringBuilder coverTempBase64;

    private BookParserConfiguration configuration;

    public Fb2ParserHandler(BookParserConfiguration configuration) {
        this.configuration = configuration;
        bookDTO = new BookDTO();
        coverTempBase64 = new StringBuilder();
    }

    public BookDTO getBookDTO() {
        return bookDTO;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "publish-info":
                isPublishInfoSection = true;
                break;

            case "title-info":
                isTitleInfoSection = true;
                break;

            case "author":
                isAuthorSection = true;
                break;

            case "sequence":
                if (configuration.isParseSeries()) {
                    bookDTO.setSeries(attributes.getValue("name"));
                }
                break;

            case "coverpage":
                isCoverSection = true;
                break;
        }

        if (isCoverSection) {
            if (qName.equalsIgnoreCase("image")) {
                coverImage = attributes.getValue("href").replace("#", "");
            }
        }

        if (qName.equalsIgnoreCase("binary") && attributes.getValue("id").equals(coverImage)) {
            isCoverImageTag = true;
        }

        thisElement = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "publish-info":
                isPublishInfoSection = false;
                break;

            case "title-info":
                isTitleInfoSection = false;
                break;

            case "author":
                isAuthorSection = false;
                break;

            case "coverpage":
                isCoverSection = false;
        }

        thisElement = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (configuration.isParseTitle() && thisElement.equalsIgnoreCase("book-title") && isTitleInfoSection) {
            bookDTO.setTitle(new String(ch, start, length));
        }

        if (configuration.isParseCover() && isCoverImageTag) {
            try {
                coverTempBase64.append(new String(ch, start, length));
            } catch (OutOfMemoryError ignored) {
                throw new NonParsableBookFileException();
            }
        }

        if (configuration.isParseGenre() && isTitleInfoSection) {
            if (thisElement.equalsIgnoreCase("genre")) {
                bookDTO.setGenre(new String(ch, start, length));
            }
        }

        if (configuration.isParseAuthor() && isTitleInfoSection && isAuthorSection) {
            switch (thisElement) {
                case "first-name":
                    bookDTO.setAuthorFirstName(new String(ch, start, length));
                    break;
                case "middle-name":
                    bookDTO.setAuthorMiddleName(new String(ch, start, length));
                    break;
                case "last-name":
                    bookDTO.setAuthorLastName(new String(ch, start, length));
                    break;
            }
        }

        if (configuration.isParseYear() && isPublishInfoSection) {
            if (thisElement.equalsIgnoreCase("year")) {
                bookDTO.setYear(new String(ch, start, length));
            }
        }
    }

    @Override
    public void endDocument() throws SAXException {
        if (!StringUtils.isEmpty(coverTempBase64)) {

            try {
                System.gc();
                bookDTO.setCover(coverTempBase64.toString());
            } catch (OutOfMemoryError ex) {
                throw new NonParsableBookFileException();
            }
        }
    }
}
