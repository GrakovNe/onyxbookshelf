package org.grakovne.onyxbookshelf.exceptions.entity;

public class IncorrectEntityException extends RuntimeException {
    public IncorrectEntityException(String message) {
        super(message);
    }
}
