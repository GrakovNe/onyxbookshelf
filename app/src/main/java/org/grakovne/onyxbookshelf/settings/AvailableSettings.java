package org.grakovne.onyxbookshelf.settings;

public enum AvailableSettings {
    LIBRARY_STARTUP_UPDATE("library_startup_update_prefs", "true"),

    LAST_RESCAN_DATE("last_rescan_date, ", "1489914513"),

    PREVIOUS_SORTING_OPTION("prev_sorting_option", "books");

    private String settingString;
    private String defaultValue;

    AvailableSettings(String settingString, String defaultValue) {
        this.settingString = settingString;
        this.defaultValue = defaultValue;
    }

    public String getPreference() {
        return settingString;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
