package org.grakovne.onyxbookshelf.settings;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SettingsManager {
    private static final String PREFERENCES_NAME = "onyx_bookshelf_shared_preferences";

    private static SettingsManager settingsManager;
    private static SharedPreferences sharedPreferences;

    private SettingsManager(Application application) {
        sharedPreferences = application.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static SettingsManager getManager(Application application) {
        if (null == settingsManager) {
            settingsManager = new SettingsManager(application);
        }
        return settingsManager;
    }

    public Boolean getLibraryStatupUpdate() {
        return Boolean.valueOf(sharedPreferences.getString(AvailableSettings.LIBRARY_STARTUP_UPDATE.getPreference(), AvailableSettings.LIBRARY_STARTUP_UPDATE.getDefaultValue()));
    }

    public void setLibraryStatupUpdate(Boolean value) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(AvailableSettings.LIBRARY_STARTUP_UPDATE.getPreference(), value.toString());
        edit.apply();
    }

    public String getLastRescanDate() {
        String timeStampStrings = sharedPreferences.getString(AvailableSettings.LAST_RESCAN_DATE.getPreference(), AvailableSettings.LAST_RESCAN_DATE.getDefaultValue());
        return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault()).format(Long.valueOf(timeStampStrings));
    }

    public void setLastRescanDate(Date date) {
        String timeStampString = String.valueOf(date.getTime());

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(AvailableSettings.LAST_RESCAN_DATE.getPreference(), timeStampString);
        edit.apply();
    }

    public void setSortingOption(String option) {
        if (StringUtils.isEmpty(option)) {
            throw new IllegalArgumentException();
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(AvailableSettings.PREVIOUS_SORTING_OPTION.getPreference(), option);
        edit.apply();
    }

    public String getSortingOption() {
        return sharedPreferences.getString(AvailableSettings.PREVIOUS_SORTING_OPTION.getPreference(), AvailableSettings.PREVIOUS_SORTING_OPTION.getDefaultValue());
    }

}
