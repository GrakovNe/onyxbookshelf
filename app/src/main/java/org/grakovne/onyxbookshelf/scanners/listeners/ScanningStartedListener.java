package org.grakovne.onyxbookshelf.scanners.listeners;

public interface ScanningStartedListener {
    void onScanningStarted();
}
