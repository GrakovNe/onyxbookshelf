package org.grakovne.onyxbookshelf.scanners.listeners;

public interface BookScannedListener {
    void onBookScanned();
}
