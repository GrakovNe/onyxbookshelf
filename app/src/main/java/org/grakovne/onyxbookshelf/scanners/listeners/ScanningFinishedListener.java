package org.grakovne.onyxbookshelf.scanners.listeners;

public interface ScanningFinishedListener {
    void onScanningFinished();
}
