package org.grakovne.onyxbookshelf.scanners;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.grakovne.onyxbookshelf.exceptions.files.IncorrectFileDirectory;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FileScanner {

    private final String[] availableFileFormats = {"fb2", "epub", "pdf", "djvu", "doc", "txt", "prc", "mobi", "doc, docx"};
    private List<String> availableFileFormatsList = Arrays.asList(availableFileFormats);

    public List<File> getBookFileRecursively(File file) {
        checkFileDirectory(file);
        return (List<File>) FileUtils.listFiles(file, availableFileFormats, true);
    }

    public List<File> retainOnlyBookFiles(List<File> files) {
        List<File> result = new ArrayList<>();

        for (File file : files) {
            if (isBookFile(file)) {
                result.add(file);
            }
        }

        return result;
    }

    public boolean isBookFile(File file) {
        return availableFileFormatsList.contains(FilenameUtils.getExtension(file.getName()));
    }

    private void checkFileDirectory(File directory) {
        if (!(null != directory && directory.exists() && directory.isDirectory())) {
            throw new IncorrectFileDirectory();
        }
    }
}
