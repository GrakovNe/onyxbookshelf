package org.grakovne.onyxbookshelf.scanners;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import org.grakovne.onyxbookshelf.db.services.BaseService;
import org.grakovne.onyxbookshelf.db.services.BookService;
import org.grakovne.onyxbookshelf.domain.Book;
import org.grakovne.onyxbookshelf.parsers.BookParserConfiguration;
import org.grakovne.onyxbookshelf.parsers.factories.BookFactory;
import org.grakovne.onyxbookshelf.scanners.listeners.BookScannedListener;
import org.grakovne.onyxbookshelf.scanners.listeners.ScanningFinishedListener;
import org.grakovne.onyxbookshelf.scanners.listeners.ScanningStartedListener;

import java.io.File;
import java.util.List;

import java9.util.stream.Collectors;
import java9.util.stream.StreamSupport;

import static org.grakovne.onyxbookshelf.Application.externalBookPath;
import static org.grakovne.onyxbookshelf.Application.internalBookBath;

public class LibraryScanner {
    private ScanningFinishedListener scanningFinishedListener;
    private BookScannedListener bookScannedListener;
    private ScanningStartedListener scanningStartedListener;
    private int booksCount = 0;

    public void setScanningFinishedListener(ScanningFinishedListener scanningFinishedListener) {
        this.scanningFinishedListener = scanningFinishedListener;
    }

    public void setBookScannedListener(BookScannedListener bookScannedListener) {
        this.bookScannedListener = bookScannedListener;
    }

    public int getBooksCount() {
        return booksCount;
    }

    public void setScanningStartedListener(ScanningStartedListener scanningStartedListener) {
        this.scanningStartedListener = scanningStartedListener;
    }

    public void startCompletelyRescan() {

        if (null != scanningStartedListener) {
            scanningStartedListener.onScanningStarted();
        }

        new BaseService().clearDataBase();

        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, List<File>> findBookTask = new AsyncTask<Void, Void, List<File>>() {
            @Override
            protected List<File> doInBackground(Void... params) {
                return findAllBookFiles();
            }

            @Override
            protected void onPostExecute(List<File> files) {
                scanBookFilesList(files, new BookParserConfiguration().getDefaultConfiguration());
            }
        };

        findBookTask.execute();
    }

    public void startBookFileListScan(List<File> files) {
        List<File> bookFiles = new FileScanner().retainOnlyBookFiles(files);

        if (null != scanningStartedListener) {
            scanningStartedListener.onScanningStarted();
        }

        scanBookFilesList(bookFiles, new BookParserConfiguration().getDefaultConfiguration());
    }

    private List<File> findAllBookFiles() {
        FileScanner scanner = new FileScanner();

        List<File> bookFiles = scanner.getBookFileRecursively(internalBookBath);
        if (externalBookPath.exists()) {
            bookFiles.addAll(scanner.getBookFileRecursively(externalBookPath));
        }

        return bookFiles;
    }

    private void scanBookFilesList(final List<File> bookFiles, final BookParserConfiguration configuration) {
        new Thread(() -> {
            List<Book> scanned = StreamSupport
                    .parallelStream(bookFiles)
                    .map(file -> new BookFactory().getBook(file, configuration))
                    .peek(book -> {
                        booksCount++;
                        if (null != bookScannedListener && booksCount % 5 == 0) {
                            bookScannedListener.onBookScanned();
                        }
                    })
                    .collect(Collectors.toList());

            new BookService().createBooks(scanned);

            if (null != scanningFinishedListener) {
                scanningFinishedListener.onScanningFinished();
            }
        }).start();
    }

}
